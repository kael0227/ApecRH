﻿namespace ApecRH
{
    partial class CandidatesAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtSalary = new MetroFramework.Controls.MetroTextBox();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.mcNationality = new MetroFramework.Controls.MetroComboBox();
            this.McPosition = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.mtDate = new MetroFramework.Controls.MetroDateTime();
            this.btnsavequit = new MetroFramework.Controls.MetroButton();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.McInactive = new MetroFramework.Controls.MetroCheckBox();
            this.McDepartment = new MetroFramework.Controls.MetroComboBox();
            this.mtNationalID = new MetroFramework.Controls.MetroTextBox();
            this.mtName = new MetroFramework.Controls.MetroTextBox();
            this.mtLastName = new MetroFramework.Controls.MetroTextBox();
            this.mtEmployee = new MetroFramework.Controls.MetroTextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtSalary);
            this.groupBox1.Controls.Add(this.metroLabel10);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.mcNationality);
            this.groupBox1.Controls.Add(this.McPosition);
            this.groupBox1.Controls.Add(this.metroLabel8);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.mtDate);
            this.groupBox1.Controls.Add(this.btnsavequit);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.McInactive);
            this.groupBox1.Controls.Add(this.McDepartment);
            this.groupBox1.Controls.Add(this.mtNationalID);
            this.groupBox1.Controls.Add(this.mtName);
            this.groupBox1.Controls.Add(this.mtLastName);
            this.groupBox1.Controls.Add(this.mtEmployee);
            this.groupBox1.Location = new System.Drawing.Point(50, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 489);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee";
            // 
            // mtSalary
            // 
            // 
            // 
            // 
            this.mtSalary.CustomButton.Image = null;
            this.mtSalary.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtSalary.CustomButton.Name = "";
            this.mtSalary.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtSalary.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtSalary.CustomButton.TabIndex = 1;
            this.mtSalary.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtSalary.CustomButton.UseSelectable = true;
            this.mtSalary.CustomButton.Visible = false;
            this.mtSalary.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "Salary", true));
            this.mtSalary.Lines = new string[0];
            this.mtSalary.Location = new System.Drawing.Point(251, 355);
            this.mtSalary.MaxLength = 32767;
            this.mtSalary.Name = "mtSalary";
            this.mtSalary.PasswordChar = '\0';
            this.mtSalary.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtSalary.SelectedText = "";
            this.mtSalary.SelectionLength = 0;
            this.mtSalary.SelectionStart = 0;
            this.mtSalary.ShortcutsEnabled = true;
            this.mtSalary.Size = new System.Drawing.Size(177, 23);
            this.mtSalary.TabIndex = 38;
            this.mtSalary.UseSelectable = true;
            this.mtSalary.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtSalary.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataSource = typeof(ApecRH.Models.Employees);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(135, 355);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(45, 19);
            this.metroLabel10.TabIndex = 37;
            this.metroLabel10.Text = "Salary";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(135, 303);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(71, 19);
            this.metroLabel9.TabIndex = 36;
            this.metroLabel9.Text = "Nationality";
            // 
            // mcNationality
            // 
            this.mcNationality.DataSource = this.employeesBindingSource;
            this.mcNationality.DisplayMember = "Nationality";
            this.mcNationality.FormattingEnabled = true;
            this.mcNationality.ItemHeight = 23;
            this.mcNationality.Location = new System.Drawing.Point(251, 303);
            this.mcNationality.Name = "mcNationality";
            this.mcNationality.Size = new System.Drawing.Size(177, 29);
            this.mcNationality.TabIndex = 35;
            this.mcNationality.UseSelectable = true;
            this.mcNationality.ValueMember = "NationalitiesID";
            // 
            // McPosition
            // 
            this.McPosition.DataSource = this.employeesBindingSource;
            this.McPosition.DisplayMember = "Position";
            this.McPosition.FormattingEnabled = true;
            this.McPosition.ItemHeight = 23;
            this.McPosition.Location = new System.Drawing.Point(251, 262);
            this.McPosition.Name = "McPosition";
            this.McPosition.Size = new System.Drawing.Size(177, 29);
            this.McPosition.TabIndex = 34;
            this.McPosition.UseSelectable = true;
            this.McPosition.ValueMember = "PositionID";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(135, 262);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(54, 19);
            this.metroLabel8.TabIndex = 33;
            this.metroLabel8.Text = "Position";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(135, 410);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(52, 19);
            this.metroLabel7.TabIndex = 32;
            this.metroLabel7.Text = "Inactive";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(135, 185);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(102, 19);
            this.metroLabel6.TabIndex = 31;
            this.metroLabel6.Text = "ApplicationDate";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(135, 140);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(70, 19);
            this.metroLabel5.TabIndex = 30;
            this.metroLabel5.Text = "NationalID";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(135, 100);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(71, 19);
            this.metroLabel4.TabIndex = 29;
            this.metroLabel4.Text = "Last Name";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(135, 62);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(45, 19);
            this.metroLabel3.TabIndex = 28;
            this.metroLabel3.Text = "Name";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(135, 27);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(69, 19);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Candidate";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(135, 222);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(87, 19);
            this.metroLabel1.TabIndex = 26;
            this.metroLabel1.Text = "Departament";
            // 
            // mtDate
            // 
            this.mtDate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "HireDate", true));
            this.mtDate.Location = new System.Drawing.Point(251, 175);
            this.mtDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.mtDate.Name = "mtDate";
            this.mtDate.Size = new System.Drawing.Size(177, 29);
            this.mtDate.TabIndex = 25;
            this.mtDate.Value = new System.DateTime(2018, 2, 2, 0, 0, 0, 0);
            // 
            // btnsavequit
            // 
            this.btnsavequit.Location = new System.Drawing.Point(518, 383);
            this.btnsavequit.Name = "btnsavequit";
            this.btnsavequit.Size = new System.Drawing.Size(100, 23);
            this.btnsavequit.TabIndex = 24;
            this.btnsavequit.Text = "Save And Quit";
            this.btnsavequit.UseSelectable = true;
            this.btnsavequit.Visible = false;
            this.btnsavequit.Click += new System.EventHandler(this.btnsavequit_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(169, 460);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(389, 460);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // McInactive
            // 
            this.McInactive.AutoSize = true;
            this.McInactive.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.employeesBindingSource, "Inactive", true));
            this.McInactive.Location = new System.Drawing.Point(251, 414);
            this.McInactive.Name = "McInactive";
            this.McInactive.Size = new System.Drawing.Size(64, 15);
            this.McInactive.TabIndex = 22;
            this.McInactive.Text = "Inactive";
            this.McInactive.UseSelectable = true;
            // 
            // McDepartment
            // 
            this.McDepartment.DataSource = this.employeesBindingSource;
            this.McDepartment.DisplayMember = "Department";
            this.McDepartment.FormattingEnabled = true;
            this.McDepartment.ItemHeight = 23;
            this.McDepartment.Location = new System.Drawing.Point(251, 222);
            this.McDepartment.Name = "McDepartment";
            this.McDepartment.Size = new System.Drawing.Size(177, 29);
            this.McDepartment.TabIndex = 21;
            this.McDepartment.UseSelectable = true;
            this.McDepartment.ValueMember = "DepartmentRefID";
            // 
            // mtNationalID
            // 
            // 
            // 
            // 
            this.mtNationalID.CustomButton.Image = null;
            this.mtNationalID.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtNationalID.CustomButton.Name = "";
            this.mtNationalID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtNationalID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtNationalID.CustomButton.TabIndex = 1;
            this.mtNationalID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtNationalID.CustomButton.UseSelectable = true;
            this.mtNationalID.CustomButton.Visible = false;
            this.mtNationalID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "NationalID", true));
            this.mtNationalID.Lines = new string[0];
            this.mtNationalID.Location = new System.Drawing.Point(251, 136);
            this.mtNationalID.MaxLength = 11;
            this.mtNationalID.Name = "mtNationalID";
            this.mtNationalID.PasswordChar = '\0';
            this.mtNationalID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtNationalID.SelectedText = "";
            this.mtNationalID.SelectionLength = 0;
            this.mtNationalID.SelectionStart = 0;
            this.mtNationalID.ShortcutsEnabled = true;
            this.mtNationalID.Size = new System.Drawing.Size(177, 23);
            this.mtNationalID.TabIndex = 20;
            this.mtNationalID.UseSelectable = true;
            this.mtNationalID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtNationalID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtNationalID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtName
            // 
            // 
            // 
            // 
            this.mtName.CustomButton.Image = null;
            this.mtName.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtName.CustomButton.Name = "";
            this.mtName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtName.CustomButton.TabIndex = 1;
            this.mtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtName.CustomButton.UseSelectable = true;
            this.mtName.CustomButton.Visible = false;
            this.mtName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "Name", true));
            this.mtName.Lines = new string[0];
            this.mtName.Location = new System.Drawing.Point(251, 62);
            this.mtName.MaxLength = 32767;
            this.mtName.Name = "mtName";
            this.mtName.PasswordChar = '\0';
            this.mtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtName.SelectedText = "";
            this.mtName.SelectionLength = 0;
            this.mtName.SelectionStart = 0;
            this.mtName.ShortcutsEnabled = true;
            this.mtName.Size = new System.Drawing.Size(177, 23);
            this.mtName.TabIndex = 19;
            this.mtName.UseSelectable = true;
            this.mtName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // mtLastName
            // 
            // 
            // 
            // 
            this.mtLastName.CustomButton.Image = null;
            this.mtLastName.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtLastName.CustomButton.Name = "";
            this.mtLastName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtLastName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtLastName.CustomButton.TabIndex = 1;
            this.mtLastName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtLastName.CustomButton.UseSelectable = true;
            this.mtLastName.CustomButton.Visible = false;
            this.mtLastName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "LastName", true));
            this.mtLastName.Lines = new string[0];
            this.mtLastName.Location = new System.Drawing.Point(251, 100);
            this.mtLastName.MaxLength = 32767;
            this.mtLastName.Name = "mtLastName";
            this.mtLastName.PasswordChar = '\0';
            this.mtLastName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtLastName.SelectedText = "";
            this.mtLastName.SelectionLength = 0;
            this.mtLastName.SelectionStart = 0;
            this.mtLastName.ShortcutsEnabled = true;
            this.mtLastName.Size = new System.Drawing.Size(177, 23);
            this.mtLastName.TabIndex = 18;
            this.mtLastName.UseSelectable = true;
            this.mtLastName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtLastName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // mtEmployee
            // 
            // 
            // 
            // 
            this.mtEmployee.CustomButton.Image = null;
            this.mtEmployee.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtEmployee.CustomButton.Name = "";
            this.mtEmployee.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtEmployee.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtEmployee.CustomButton.TabIndex = 1;
            this.mtEmployee.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtEmployee.CustomButton.UseSelectable = true;
            this.mtEmployee.CustomButton.Visible = false;
            this.mtEmployee.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.employeesBindingSource, "EmployeeID", true));
            this.mtEmployee.Lines = new string[0];
            this.mtEmployee.Location = new System.Drawing.Point(251, 27);
            this.mtEmployee.MaxLength = 32767;
            this.mtEmployee.Name = "mtEmployee";
            this.mtEmployee.PasswordChar = '\0';
            this.mtEmployee.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtEmployee.SelectedText = "";
            this.mtEmployee.SelectionLength = 0;
            this.mtEmployee.SelectionStart = 0;
            this.mtEmployee.ShortcutsEnabled = true;
            this.mtEmployee.Size = new System.Drawing.Size(177, 23);
            this.mtEmployee.TabIndex = 17;
            this.mtEmployee.UseSelectable = true;
            this.mtEmployee.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtEmployee.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtEmployee.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // CandidatesAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 587);
            this.Controls.Add(this.groupBox1);
            this.Name = "CandidatesAddAndEdit";
            this.Text = "CandidatesAddAndEdit";
            this.Load += new System.EventHandler(this.EmployeesAddAndEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        public MetroFramework.Controls.MetroButton btnsavequit;
        private MetroFramework.Controls.MetroButton BtnCancel;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroCheckBox McInactive;
        private MetroFramework.Controls.MetroComboBox McDepartment;
        private MetroFramework.Controls.MetroTextBox mtNationalID;
        private MetroFramework.Controls.MetroTextBox mtName;
        private MetroFramework.Controls.MetroTextBox mtLastName;
        public MetroFramework.Controls.MetroTextBox mtEmployee;
        private MetroFramework.Controls.MetroDateTime mtDate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox McPosition;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox mcNationality;
        private MetroFramework.Controls.MetroTextBox mtSalary;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.BindingSource employeesBindingSource;
    }
}