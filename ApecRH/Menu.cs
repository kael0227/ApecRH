﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class Menu : MetroFramework.Forms.MetroForm
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void mtadm_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuAdministration frm = new MenuAdministration();
            frm.Show();
            this.Dispose();
        }

        private void mtcolaboradores_Click(object sender, EventArgs e)
        {
            FormEmployee frm = new FormEmployee();
            this.Hide();
            frm.ShowDialog();
            this.Dispose();
        }

        private void mtRecruitment_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(MenuRecruiment frm = new MenuRecruiment())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
