﻿namespace ApecRH
{
    partial class CandidatesEducationAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtCareer = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.MtDateEnd = new MetroFramework.Controls.MetroDateTime();
            this.MtDateBeg = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnsavequit = new MetroFramework.Controls.MetroButton();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.MtLevel = new MetroFramework.Controls.MetroComboBox();
            this.mtCandidate = new MetroFramework.Controls.MetroTextBox();
            this.MtInstitute = new MetroFramework.Controls.MetroTextBox();
            this.mtcandwork = new MetroFramework.Controls.MetroTextBox();
            this.candidatesEducationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesEducationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtCareer);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.MtDateEnd);
            this.groupBox1.Controls.Add(this.MtDateBeg);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.MtLevel);
            this.groupBox1.Controls.Add(this.mtCandidate);
            this.groupBox1.Controls.Add(this.MtInstitute);
            this.groupBox1.Controls.Add(this.mtcandwork);
            this.groupBox1.Location = new System.Drawing.Point(161, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 444);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "WorkExp";
            // 
            // mtCareer
            // 
            // 
            // 
            // 
            this.mtCareer.CustomButton.Image = null;
            this.mtCareer.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtCareer.CustomButton.Name = "";
            this.mtCareer.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtCareer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtCareer.CustomButton.TabIndex = 1;
            this.mtCareer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtCareer.CustomButton.UseSelectable = true;
            this.mtCareer.CustomButton.Visible = false;
            this.mtCareer.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesEducationBindingSource, "Career", true));
            this.mtCareer.Lines = new string[0];
            this.mtCareer.Location = new System.Drawing.Point(191, 183);
            this.mtCareer.MaxLength = 32767;
            this.mtCareer.Name = "mtCareer";
            this.mtCareer.PasswordChar = '\0';
            this.mtCareer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtCareer.SelectedText = "";
            this.mtCareer.SelectionLength = 0;
            this.mtCareer.SelectionStart = 0;
            this.mtCareer.ShortcutsEnabled = true;
            this.mtCareer.Size = new System.Drawing.Size(177, 23);
            this.mtCareer.TabIndex = 35;
            this.mtCareer.UseSelectable = true;
            this.mtCareer.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtCareer.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtCareer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(71, 183);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(49, 19);
            this.metroLabel7.TabIndex = 34;
            this.metroLabel7.Text = "Career";
            // 
            // MtDateEnd
            // 
            this.MtDateEnd.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesEducationBindingSource, "DateEnd", true));
            this.MtDateEnd.Location = new System.Drawing.Point(192, 322);
            this.MtDateEnd.MinimumSize = new System.Drawing.Size(0, 29);
            this.MtDateEnd.Name = "MtDateEnd";
            this.MtDateEnd.Size = new System.Drawing.Size(178, 29);
            this.MtDateEnd.TabIndex = 33;
            // 
            // MtDateBeg
            // 
            this.MtDateBeg.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesEducationBindingSource, "DateBeg", true));
            this.MtDateBeg.Location = new System.Drawing.Point(191, 275);
            this.MtDateBeg.MinimumSize = new System.Drawing.Size(0, 29);
            this.MtDateBeg.Name = "MtDateBeg";
            this.MtDateBeg.Size = new System.Drawing.Size(178, 29);
            this.MtDateBeg.TabIndex = 32;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(72, 322);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(58, 19);
            this.metroLabel6.TabIndex = 30;
            this.metroLabel6.Text = "DateEnd";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(71, 229);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(38, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Level";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(72, 275);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(59, 19);
            this.metroLabel4.TabIndex = 28;
            this.metroLabel4.Text = "DateBeg";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(71, 140);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(53, 19);
            this.metroLabel3.TabIndex = 27;
            this.metroLabel3.Text = "Institute";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(71, 95);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(81, 19);
            this.metroLabel2.TabIndex = 26;
            this.metroLabel2.Text = "CandidateID";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(71, 45);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(73, 19);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "WorkExpID";
            this.metroLabel1.Visible = false;
            // 
            // btnsavequit
            // 
            this.btnsavequit.Location = new System.Drawing.Point(699, 381);
            this.btnsavequit.Name = "btnsavequit";
            this.btnsavequit.Size = new System.Drawing.Size(100, 23);
            this.btnsavequit.TabIndex = 24;
            this.btnsavequit.Text = "Save And Quit";
            this.btnsavequit.UseSelectable = true;
            this.btnsavequit.Visible = false;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(129, 386);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(263, 386);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // MtLevel
            // 
            this.MtLevel.FormattingEnabled = true;
            this.MtLevel.ItemHeight = 23;
            this.MtLevel.Items.AddRange(new object[] {
            "",
            "Grado",
            "Post-Grado",
            "Maestria",
            "Doctorado"});
            this.MtLevel.Location = new System.Drawing.Point(192, 229);
            this.MtLevel.Name = "MtLevel";
            this.MtLevel.Size = new System.Drawing.Size(177, 29);
            this.MtLevel.TabIndex = 21;
            this.MtLevel.UseSelectable = true;
            // 
            // mtCandidate
            // 
            // 
            // 
            // 
            this.mtCandidate.CustomButton.Image = null;
            this.mtCandidate.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtCandidate.CustomButton.Name = "";
            this.mtCandidate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtCandidate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtCandidate.CustomButton.TabIndex = 1;
            this.mtCandidate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtCandidate.CustomButton.UseSelectable = true;
            this.mtCandidate.CustomButton.Visible = false;
            this.mtCandidate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesEducationBindingSource, "CandidateID", true));
            this.mtCandidate.Lines = new string[0];
            this.mtCandidate.Location = new System.Drawing.Point(191, 95);
            this.mtCandidate.MaxLength = 32767;
            this.mtCandidate.Name = "mtCandidate";
            this.mtCandidate.PasswordChar = '\0';
            this.mtCandidate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtCandidate.SelectedText = "";
            this.mtCandidate.SelectionLength = 0;
            this.mtCandidate.SelectionStart = 0;
            this.mtCandidate.ShortcutsEnabled = true;
            this.mtCandidate.Size = new System.Drawing.Size(177, 23);
            this.mtCandidate.TabIndex = 19;
            this.mtCandidate.UseSelectable = true;
            this.mtCandidate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtCandidate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtCandidate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // MtInstitute
            // 
            // 
            // 
            // 
            this.MtInstitute.CustomButton.Image = null;
            this.MtInstitute.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.MtInstitute.CustomButton.Name = "";
            this.MtInstitute.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.MtInstitute.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.MtInstitute.CustomButton.TabIndex = 1;
            this.MtInstitute.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MtInstitute.CustomButton.UseSelectable = true;
            this.MtInstitute.CustomButton.Visible = false;
            this.MtInstitute.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesEducationBindingSource, "Institute", true));
            this.MtInstitute.Lines = new string[0];
            this.MtInstitute.Location = new System.Drawing.Point(191, 140);
            this.MtInstitute.MaxLength = 32767;
            this.MtInstitute.Name = "MtInstitute";
            this.MtInstitute.PasswordChar = '\0';
            this.MtInstitute.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.MtInstitute.SelectedText = "";
            this.MtInstitute.SelectionLength = 0;
            this.MtInstitute.SelectionStart = 0;
            this.MtInstitute.ShortcutsEnabled = true;
            this.MtInstitute.Size = new System.Drawing.Size(177, 23);
            this.MtInstitute.TabIndex = 18;
            this.MtInstitute.UseSelectable = true;
            this.MtInstitute.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.MtInstitute.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.MtInstitute.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // mtcandwork
            // 
            // 
            // 
            // 
            this.mtcandwork.CustomButton.Image = null;
            this.mtcandwork.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtcandwork.CustomButton.Name = "";
            this.mtcandwork.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcandwork.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcandwork.CustomButton.TabIndex = 1;
            this.mtcandwork.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcandwork.CustomButton.UseSelectable = true;
            this.mtcandwork.CustomButton.Visible = false;
            this.mtcandwork.Lines = new string[0];
            this.mtcandwork.Location = new System.Drawing.Point(191, 45);
            this.mtcandwork.MaxLength = 32767;
            this.mtcandwork.Name = "mtcandwork";
            this.mtcandwork.PasswordChar = '\0';
            this.mtcandwork.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcandwork.SelectedText = "";
            this.mtcandwork.SelectionLength = 0;
            this.mtcandwork.SelectionStart = 0;
            this.mtcandwork.ShortcutsEnabled = true;
            this.mtcandwork.Size = new System.Drawing.Size(177, 23);
            this.mtcandwork.TabIndex = 17;
            this.mtcandwork.UseSelectable = true;
            this.mtcandwork.Visible = false;
            this.mtcandwork.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcandwork.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // candidatesEducationBindingSource
            // 
            this.candidatesEducationBindingSource.DataSource = typeof(ApecRH.Models.CandidatesEducation);
            // 
            // CandidatesEducationAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 618);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnsavequit);
            this.Name = "CandidatesEducationAddAndEdit";
            this.Text = "CandidatesEducationAddAndEdit";
            this.Load += new System.EventHandler(this.CandidatesEducationAddAndEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesEducationBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroDateTime MtDateEnd;
        private MetroFramework.Controls.MetroDateTime MtDateBeg;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroButton btnsavequit;
        private MetroFramework.Controls.MetroButton BtnCancel;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroComboBox MtLevel;
        private MetroFramework.Controls.MetroTextBox mtCandidate;
        private MetroFramework.Controls.MetroTextBox MtInstitute;
        public MetroFramework.Controls.MetroTextBox mtcandwork;
        private System.Windows.Forms.BindingSource candidatesEducationBindingSource;
        private MetroFramework.Controls.MetroTextBox mtCareer;
        private MetroFramework.Controls.MetroLabel metroLabel7;
    }
}