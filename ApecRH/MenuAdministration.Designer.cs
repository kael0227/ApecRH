﻿namespace ApecRH
{
    partial class MenuAdministration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtDepartments = new MetroFramework.Controls.MetroTile();
            this.mtPosition = new MetroFramework.Controls.MetroTile();
            this.mtgoback = new MetroFramework.Controls.MetroTile();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtgoback);
            this.groupBox1.Controls.Add(this.mtDepartments);
            this.groupBox1.Controls.Add(this.mtPosition);
            this.groupBox1.Location = new System.Drawing.Point(23, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 231);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // mtDepartments
            // 
            this.mtDepartments.ActiveControl = null;
            this.mtDepartments.BackColor = System.Drawing.Color.Crimson;
            this.mtDepartments.Location = new System.Drawing.Point(190, 29);
            this.mtDepartments.Name = "mtDepartments";
            this.mtDepartments.Size = new System.Drawing.Size(113, 82);
            this.mtDepartments.TabIndex = 1;
            this.mtDepartments.Text = "Departments";
            this.mtDepartments.UseCustomBackColor = true;
            this.mtDepartments.UseSelectable = true;
            this.mtDepartments.Click += new System.EventHandler(this.mtDepartments_Click);
            // 
            // mtPosition
            // 
            this.mtPosition.ActiveControl = null;
            this.mtPosition.BackColor = System.Drawing.Color.Chocolate;
            this.mtPosition.Location = new System.Drawing.Point(25, 29);
            this.mtPosition.Name = "mtPosition";
            this.mtPosition.Size = new System.Drawing.Size(113, 82);
            this.mtPosition.TabIndex = 0;
            this.mtPosition.Text = "Position";
            this.mtPosition.UseCustomBackColor = true;
            this.mtPosition.UseSelectable = true;
            this.mtPosition.Click += new System.EventHandler(this.mtPosition_Click);
            // 
            // mtgoback
            // 
            this.mtgoback.ActiveControl = null;
            this.mtgoback.BackColor = System.Drawing.Color.CornflowerBlue;
            this.mtgoback.Location = new System.Drawing.Point(108, 131);
            this.mtgoback.Name = "mtgoback";
            this.mtgoback.Size = new System.Drawing.Size(113, 82);
            this.mtgoback.TabIndex = 2;
            this.mtgoback.Text = "Go Back";
            this.mtgoback.UseCustomBackColor = true;
            this.mtgoback.UseSelectable = true;
            this.mtgoback.Click += new System.EventHandler(this.mtgoback_Click);
            // 
            // MenuAdministration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 317);
            this.Controls.Add(this.groupBox1);
            this.Name = "MenuAdministration";
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "Administration";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTile mtDepartments;
        private MetroFramework.Controls.MetroTile mtPosition;
        private MetroFramework.Controls.MetroTile mtgoback;
    }
}