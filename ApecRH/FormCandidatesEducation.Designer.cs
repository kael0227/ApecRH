﻿namespace ApecRH
{
    partial class FormCandidatesEducation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mtcanfilter = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Candidate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Career = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtgoback = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.candidateEducationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidateIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.instituteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateBegDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateEndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidatesEducationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesEducationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mtcanfilter
            // 
            // 
            // 
            // 
            this.mtcanfilter.CustomButton.Image = null;
            this.mtcanfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtcanfilter.CustomButton.Name = "";
            this.mtcanfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcanfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcanfilter.CustomButton.TabIndex = 1;
            this.mtcanfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcanfilter.CustomButton.UseSelectable = true;
            this.mtcanfilter.CustomButton.Visible = false;
            this.mtcanfilter.Lines = new string[0];
            this.mtcanfilter.Location = new System.Drawing.Point(517, 63);
            this.mtcanfilter.MaxLength = 32767;
            this.mtcanfilter.Name = "mtcanfilter";
            this.mtcanfilter.PasswordChar = '\0';
            this.mtcanfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcanfilter.SelectedText = "";
            this.mtcanfilter.SelectionLength = 0;
            this.mtcanfilter.SelectionStart = 0;
            this.mtcanfilter.ShortcutsEnabled = true;
            this.mtcanfilter.Size = new System.Drawing.Size(121, 23);
            this.mtcanfilter.TabIndex = 29;
            this.mtcanfilter.UseSelectable = true;
            this.mtcanfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcanfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(442, 63);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(69, 19);
            this.metroLabel1.TabIndex = 27;
            this.metroLabel1.Text = "Candidate";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.candidateEducationDataGridViewTextBoxColumn,
            this.candidateIDDataGridViewTextBoxColumn,
            this.Candidate,
            this.instituteDataGridViewTextBoxColumn,
            this.Career,
            this.levelDataGridViewTextBoxColumn,
            this.dateBegDataGridViewTextBoxColumn,
            this.dateEndDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.candidatesEducationBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(5, 161);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(892, 384);
            this.dataGridView1.TabIndex = 26;
            // 
            // Candidate
            // 
            this.Candidate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Candidate.DataPropertyName = "Name";
            this.Candidate.HeaderText = "Candidate";
            this.Candidate.Name = "Candidate";
            this.Candidate.ReadOnly = true;
            // 
            // Career
            // 
            this.Career.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Career.DataPropertyName = "Career";
            this.Career.HeaderText = "Career";
            this.Career.Name = "Career";
            this.Career.ReadOnly = true;
            // 
            // mtgoback
            // 
            this.mtgoback.ActiveControl = null;
            this.mtgoback.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtgoback.Location = new System.Drawing.Point(352, 85);
            this.mtgoback.Name = "mtgoback";
            this.mtgoback.Size = new System.Drawing.Size(75, 70);
            this.mtgoback.TabIndex = 25;
            this.mtgoback.Text = "Go Back";
            this.mtgoback.UseCustomBackColor = true;
            this.mtgoback.UseSelectable = true;
            this.mtgoback.Click += new System.EventHandler(this.mtgoback_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(190, 85);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 24;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(271, 85);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 70);
            this.mtDelete.TabIndex = 23;
            this.mtDelete.Text = "Delete";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(109, 84);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 22;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(28, 84);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 21;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.mtSearch_Click);
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(517, 109);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 43;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(442, 109);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 42;
            this.metroLabel2.Text = "Status";
            // 
            // candidateEducationDataGridViewTextBoxColumn
            // 
            this.candidateEducationDataGridViewTextBoxColumn.DataPropertyName = "CandidateEducation";
            this.candidateEducationDataGridViewTextBoxColumn.HeaderText = "CandidateEducation";
            this.candidateEducationDataGridViewTextBoxColumn.Name = "candidateEducationDataGridViewTextBoxColumn";
            this.candidateEducationDataGridViewTextBoxColumn.ReadOnly = true;
            this.candidateEducationDataGridViewTextBoxColumn.Visible = false;
            // 
            // candidateIDDataGridViewTextBoxColumn
            // 
            this.candidateIDDataGridViewTextBoxColumn.DataPropertyName = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.HeaderText = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.Name = "candidateIDDataGridViewTextBoxColumn";
            this.candidateIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // instituteDataGridViewTextBoxColumn
            // 
            this.instituteDataGridViewTextBoxColumn.DataPropertyName = "Institute";
            this.instituteDataGridViewTextBoxColumn.HeaderText = "Institute";
            this.instituteDataGridViewTextBoxColumn.Name = "instituteDataGridViewTextBoxColumn";
            this.instituteDataGridViewTextBoxColumn.ReadOnly = true;
            this.instituteDataGridViewTextBoxColumn.Width = 150;
            // 
            // levelDataGridViewTextBoxColumn
            // 
            this.levelDataGridViewTextBoxColumn.DataPropertyName = "Level";
            this.levelDataGridViewTextBoxColumn.HeaderText = "Level";
            this.levelDataGridViewTextBoxColumn.Name = "levelDataGridViewTextBoxColumn";
            this.levelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateBegDataGridViewTextBoxColumn
            // 
            this.dateBegDataGridViewTextBoxColumn.DataPropertyName = "DateBeg";
            this.dateBegDataGridViewTextBoxColumn.HeaderText = "DateBeg";
            this.dateBegDataGridViewTextBoxColumn.Name = "dateBegDataGridViewTextBoxColumn";
            this.dateBegDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateEndDataGridViewTextBoxColumn
            // 
            this.dateEndDataGridViewTextBoxColumn.DataPropertyName = "DateEnd";
            this.dateEndDataGridViewTextBoxColumn.HeaderText = "DateEnd";
            this.dateEndDataGridViewTextBoxColumn.Name = "dateEndDataGridViewTextBoxColumn";
            this.dateEndDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // candidatesEducationBindingSource
            // 
            this.candidatesEducationBindingSource.DataSource = typeof(ApecRH.Models.CandidatesEducation);
            // 
            // FormCandidatesEducation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 568);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.mtcanfilter);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtgoback);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormCandidatesEducation";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Education";
            this.Load += new System.EventHandler(this.FormCandidatesEducation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesEducationBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox mtcanfilter;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroTile mtgoback;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtAddP;
        private MetroFramework.Controls.MetroTile mtSearch;
        private System.Windows.Forms.BindingSource candidatesEducationBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateEducationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Candidate;
        private System.Windows.Forms.DataGridViewTextBoxColumn instituteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Career;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateBegDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateEndDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}