﻿namespace ApecRH
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtadm = new MetroFramework.Controls.MetroTile();
            this.mtRecruitment = new MetroFramework.Controls.MetroTile();
            this.mtcolaboradores = new MetroFramework.Controls.MetroTile();
            this.label1 = new System.Windows.Forms.Label();
            this.mtSalir = new MetroFramework.Controls.MetroTile();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtadm);
            this.groupBox1.Controls.Add(this.mtRecruitment);
            this.groupBox1.Controls.Add(this.mtcolaboradores);
            this.groupBox1.Location = new System.Drawing.Point(94, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(527, 297);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // mtadm
            // 
            this.mtadm.ActiveControl = null;
            this.mtadm.BackColor = System.Drawing.Color.DarkOrange;
            this.mtadm.Location = new System.Drawing.Point(358, 30);
            this.mtadm.Name = "mtadm";
            this.mtadm.Size = new System.Drawing.Size(120, 98);
            this.mtadm.TabIndex = 3;
            this.mtadm.Text = "Administration";
            this.mtadm.UseCustomBackColor = true;
            this.mtadm.UseSelectable = true;
            this.mtadm.Click += new System.EventHandler(this.mtadm_Click);
            // 
            // mtRecruitment
            // 
            this.mtRecruitment.ActiveControl = null;
            this.mtRecruitment.BackColor = System.Drawing.Color.DarkRed;
            this.mtRecruitment.Location = new System.Drawing.Point(195, 30);
            this.mtRecruitment.Name = "mtRecruitment";
            this.mtRecruitment.Size = new System.Drawing.Size(120, 98);
            this.mtRecruitment.TabIndex = 2;
            this.mtRecruitment.Text = "Recruitment";
            this.mtRecruitment.TileImage = ((System.Drawing.Image)(resources.GetObject("mtRecruitment.TileImage")));
            this.mtRecruitment.UseCustomBackColor = true;
            this.mtRecruitment.UseSelectable = true;
            this.mtRecruitment.Click += new System.EventHandler(this.mtRecruitment_Click);
            // 
            // mtcolaboradores
            // 
            this.mtcolaboradores.ActiveControl = null;
            this.mtcolaboradores.BackColor = System.Drawing.Color.DarkSlateGray;
            this.mtcolaboradores.Location = new System.Drawing.Point(32, 30);
            this.mtcolaboradores.Name = "mtcolaboradores";
            this.mtcolaboradores.Size = new System.Drawing.Size(120, 98);
            this.mtcolaboradores.TabIndex = 0;
            this.mtcolaboradores.Text = "Associates";
            this.mtcolaboradores.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.mtcolaboradores.UseCustomBackColor = true;
            this.mtcolaboradores.UseSelectable = true;
            this.mtcolaboradores.UseTileImage = true;
            this.mtcolaboradores.Click += new System.EventHandler(this.mtcolaboradores_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(257, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "UNAPEC RH";
            // 
            // mtSalir
            // 
            this.mtSalir.ActiveControl = null;
            this.mtSalir.BackColor = System.Drawing.Color.Red;
            this.mtSalir.Location = new System.Drawing.Point(509, 34);
            this.mtSalir.Name = "mtSalir";
            this.mtSalir.Size = new System.Drawing.Size(90, 72);
            this.mtSalir.TabIndex = 3;
            this.mtSalir.Text = "Exit";
            this.mtSalir.UseCustomBackColor = true;
            this.mtSalir.UseSelectable = true;
            this.mtSalir.Click += new System.EventHandler(this.mtSalir_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 450);
            this.Controls.Add(this.mtSalir);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Menu";
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Menu";
            this.TransparencyKey = System.Drawing.Color.Indigo;
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTile mtadm;
        private MetroFramework.Controls.MetroTile mtRecruitment;
        private MetroFramework.Controls.MetroTile mtcolaboradores;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTile mtSalir;
    }
}