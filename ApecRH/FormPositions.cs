﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ApecRH
{
    public partial class FormPositions : MetroFramework.Forms.MetroForm
    {
        SqlConnection conn = null;
      
        public FormPositions()
        {
            InitializeComponent();
        }

        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(PositionsAddAndEdit frm = new PositionsAddAndEdit())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }
        
        private void FormPositions_Load(object sender, EventArgs e)
        {
            
            mcInactivefilter.SelectedItem = "Active";
           

            using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
            {

                var result = from Positions in db.Positions
                             join Risks in db.Risks on Positions.RiskID equals Risks.ID
                             where Positions.Inactive == false
                             select new
                             {
                                 Positions.PositionID,
                                 Positions.Position,
                                 Positions.SalMin,
                                 Positions.SalMax,
                                 Positions.RiskID,
                                 Positions.Inactive,
                                 Risks.RiskLevel
                             };

                dataGridView1.DataSource = result.ToList();

            }
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            ApecRH.Models.Model1 db = new ApecRH.Models.Model1();

            var result = from Positions in db.Positions
                         join Risks in db.Risks on Positions.RiskID equals Risks.ID
                         select new
                         {
                             Positions.PositionID,
                             Positions.Position,
                             Positions.SalMin,
                             Positions.SalMax,
                             Positions.RiskID,
                             Positions.Inactive,
                             Risks.RiskLevel
                         };



            if (mtposfilter.Text != "")
            {
                string posWhere = mtposfilter.Text;

                result = result.Where(Positions => Positions.PositionID.ToString().Contains(posWhere) ||
                                                   Positions.Position.Contains(posWhere));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtposfilter.Text == "")
            {

                dataGridView1.DataSource = result.ToList();

            }

            if (mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(Positions => (Positions.Inactive == true));
                dataGridView1.DataSource = result.ToList();

            }

            else if (mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(Positions => (Positions.Inactive == false));
                dataGridView1.DataSource = result.ToList();

            }

            

        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr =MessageBox.Show("Are you sure do you want to delete?","Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using(ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {
                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        if(db.Candidates.Any(p => p.PositionID == toBeDeleted))
                        {
                            MessageBox.Show("Can't erased record. It has dependency");
                            return;
                        }
                       
                        var PosD = db.Positions.First(c => c.PositionID == toBeDeleted);
                        bool inactiveD = (from d in db.Positions
                                          where d.PositionID == toBeDeleted
                                          select d.Inactive).Single();
                        if (inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar datos Inactivos. Contacte al supervisor");
                            return;
                        }

                        db.Positions.Remove(PosD);
                        db.SaveChanges();
                        var result = from Positions in db.Positions
                                     join Risks in db.Risks on Positions.RiskID equals Risks.ID
                                     where Positions.Inactive == false
                                     select new
                                     {
                                         Positions.PositionID,
                                         Positions.Position,
                                         Positions.SalMin,
                                         Positions.SalMax,
                                         Positions.RiskID,
                                         Positions.Inactive,
                                         Risks.RiskLevel
                                     };
                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }
           
            catch (Exception)
            {

                throw;
            }
        }

        private void mtEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            PositionsAddAndEdit frm = new PositionsAddAndEdit();
            frm.posID = int.Parse(row.Cells[0].Value.ToString());
            frm.posName = row.Cells[1].Value.ToString();
            frm.posSalMin = double.Parse(row.Cells[2].Value.ToString());
            frm.posSalMax = double.Parse(row.Cells[3].Value.ToString());
            frm.posRisk = int.Parse(row.Cells[4].Value.ToString());
            frm.inactive = bool.Parse(row.Cells[6].Value.ToString());
            frm.editp = true;
            frm.btnsavequit.Hide();
            frm.mtPosID.Enabled = false;
            this.Hide();
            frm.ShowDialog();
            this.Dispose();
        }

        private void mtGoback_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuAdministration frm = new MenuAdministration();
            frm.ShowDialog();
            this.Dispose();
        }
    }
}
