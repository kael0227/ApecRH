﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{


    public partial class Login : MetroFramework.Forms.MetroForm
    {
        public string user;
        public string pass;
        public bool inactive;
        public Login()
        {
            InitializeComponent();
        }

        private void Logbtn_Click(object sender, EventArgs e)
        {
            ApecRH.Models.Model1 db = new ApecRH.Models.Model1();
            user = txtuser.Text;
            pass = txtpass.Text;
            if(db.Users.Any(u => u.UserName == user && u.Password == pass && u.Inactive == false))
            {
                ApecRH.Menu frm = new ApecRH.Menu();
                this.Hide();
                frm.ShowDialog();
                this.Dispose();

                
            }
            else if(db.Users.Any(u => u.UserName == user && u.Password == pass && u.Inactive == true))
            {
                MessageBox.Show("User is inactive. Contact supervisor pls");

            }
            else
            {
                MessageBox.Show("Incorrect pass or user");
            }
                   
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
