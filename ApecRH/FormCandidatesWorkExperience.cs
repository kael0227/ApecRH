﻿using ApecRH.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class FormCandidatesWorkExperience : MetroFramework.Forms.MetroForm
    {
        public FormCandidatesWorkExperience()
        {
            InitializeComponent();
        }

        private void FormCandidatesWorkExperience_Load(object sender, EventArgs e)
        {
            using (Model1 db = new Model1())
            {
                mcInactivefilter.SelectedItem = "Active";
                var result = from cwe in db.CandidatesWorkExperience
                             join c in db.Candidates on cwe.CandidateID equals c.CandidateID
                             join p in db.Positions on cwe.PositionID equals p.PositionID
                             where c.Inactive == false
                             select new
                             {
                                 cwe.CandidateWorkExperience,
                                 cwe.CandidateID,
                                 Name = c.Name + " " + c.LastName ,
                                 cwe.Organization,
                                 cwe.PositionID,
                                 p.Position,
                                 cwe.DateBeg,
                                 cwe.DateEnd,
                                 cwe.Salary,
                                 c.Inactive

                             };
                dataGridView1.DataSource = result.ToList();
            }
        }

        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(CandidatesWorkExperienceAddAndEdit frm = new CandidatesWorkExperienceAddAndEdit())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtSearch_Click(object sender, EventArgs e)
        {
            Model1 db = new Model1();

            var result = from cwe in db.CandidatesWorkExperience
                         join c in db.Candidates on cwe.CandidateID equals c.CandidateID
                         join p in db.Positions on cwe.PositionID equals p.PositionID
                         select new
                         {
                             cwe.CandidateWorkExperience,
                             cwe.CandidateID,
                             Name = c.Name + " " + c.LastName,
                             cwe.Organization,
                             cwe.PositionID,
                             p.Position,
                             cwe.DateBeg,
                             cwe.DateEnd,
                             cwe.Salary,
                             c.Inactive,

                         };
            if(mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(nw => nw.Inactive == false);
                dataGridView1.DataSource = result.ToList();
            }
            else if(mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(nw => nw.Inactive == true);
                dataGridView1.DataSource = result.ToList();
            }
            if (mtcanfilter.Text != "")
            {

                result = result.Where(c => c.CandidateID.ToString().Contains(mtcanfilter.Text) ||
                                  c.Name.Contains(mtcanfilter.Text));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtcanfilter.Text == "")
            {
                dataGridView1.DataSource = result.ToList();
            }
        }

        private void mtEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            CandidatesWorkExperienceAddAndEdit frm = new CandidatesWorkExperienceAddAndEdit();
            frm.CandweID = int.Parse(row.Cells[0].Value.ToString());
            frm.CandidateID = int.Parse(row.Cells[1].Value.ToString());
            frm.Org = row.Cells[3].Value.ToString();
            frm.PosID = int.Parse(row.Cells[4].Value.ToString());
            frm.DateBeg = DateTime.Parse(row.Cells[6].Value.ToString());
            frm.DateEnd = DateTime.Parse(row.Cells[7].Value.ToString());
            frm.Salary = double.Parse(row.Cells[8].Value.ToString());
            frm.edit = true;
            this.Hide();
            frm.ShowDialog();
            this.Dispose();


        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to delete this record?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {

                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        var CanWeD = db.CandidatesWorkExperience.First(c => c.CandidateWorkExperience == toBeDeleted);
                        var notToBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[1].Value;
                        bool inactiveD = (from d in db.Candidates
                                          where d.CandidateID == notToBeDeleted
                                          select d.Inactive).Single();
                        if (inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar datos Inactivos. Contacte al supervisor");
                            return;
                        }
                        db.CandidatesWorkExperience.Remove(CanWeD);
                        db.SaveChanges();
                        var result = from cwe in db.CandidatesWorkExperience
                                     join c in db.Candidates on cwe.CandidateID equals c.CandidateID
                                     join p in db.Positions on cwe.PositionID equals p.PositionID
                                     where c.Inactive == false
                                     select new
                                     {
                                         cwe.CandidateWorkExperience,
                                         cwe.CandidateID,
                                         Name = c.Name + " " + c.LastName,
                                         cwe.Organization,
                                         cwe.PositionID,
                                         p.Position,
                                         cwe.DateBeg,
                                         cwe.DateEnd,
                                         cwe.Salary

                                     };
                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void mtGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuRecruiment frm = new MenuRecruiment();
            frm.ShowDialog();
            this.Hide();
        }
    }
}
