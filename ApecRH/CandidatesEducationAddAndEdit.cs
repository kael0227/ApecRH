﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;
namespace ApecRH
{
    public partial class CandidatesEducationAddAndEdit : MetroFramework.Forms.MetroForm
    {
        public int CandeID;
        public int CandidateID;
        public string Institute;
        public string Career;
        public string Level;
        public DateTime DateBeg;
        public DateTime DateEnd;
        public bool edit = false;
        public CandidatesEducationAddAndEdit()
        {
            InitializeComponent();
        }

        private void CandidatesEducationAddAndEdit_Load(object sender, EventArgs e)
        {
            
           
            using (Model1 db = new Model1())
            {
                if(!db.Candidates.Any(c => c.Inactive == false))
                {
                    MessageBox.Show("There's no any Candidate please insert one.");
                    FormCandidates frm = new FormCandidates();
                    this.Hide();
                    frm.ShowDialog();
                    this.Dispose();
                }
                var query = from c in db.Candidates
                            where c.Inactive == false
                            select c.CandidateID.ToString();



                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                source.AddRange(query.ToArray());
                this.mtCandidate.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.mtCandidate.AutoCompleteSource = AutoCompleteSource.CustomSource;
                this.mtCandidate.AutoCompleteCustomSource = source;

                if (edit == true)
                {
                    mtcandwork.Text = CandeID.ToString();
                    mtCandidate.Text = CandidateID.ToString();
                    MtInstitute.Text = Institute;
                    mtCareer.Text = Career;
                    MtLevel.Text = Level.ToString();
                    MtDateBeg.Text = DateBeg.ToString();
                    MtDateEnd.Text = DateEnd.ToString();

                }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (edit == false)
            {
                try
                {

                    CandidateID = int.Parse(mtCandidate.Text);
                    Institute = MtInstitute.Text;
                    Career = mtCareer.Text;
                    Level = MtLevel.SelectedItem.ToString();
                    DateBeg = DateTime.Parse(MtDateBeg.Text);
                    DateEnd = DateTime.Parse(MtDateEnd.Text);
                    

                    CandidatesEducation cwe = new CandidatesEducation();


                    cwe.CandidateID = CandidateID;
                    cwe.Institute = Institute;
                    cwe.Career = Career;
                    cwe.Level = Level;
                    cwe.DateBeg = DateBeg;
                    cwe.DateEnd = DateEnd;
                    if (DateBeg > DateEnd)
                    {

                        MessageBox.Show("DateBeg cannot be greater than DateEnd.");
                        return;
                    }

                    if (mtCandidate.Text == "" || mtCareer.Text == "" || MtInstitute.Text == "")
                    {

                        MessageBox.Show("Please fill all the textbox");
                        return;

                    }

                    using (Model1 db = new Model1())
                    {
                        db.CandidatesEducation.Add(cwe);
                        db.SaveChanges();
                        db.Dispose();
                        MessageBox.Show("Saved");
                    }
                    clear();

                }
                catch (Exception ex)
                {

                    string errormessage = ex.Message;
                }



            }
            else
            {


                using (Model1 db = new Model1())
                {
                    var CanweU = db.CandidatesEducation.Where(x => x.CandidateEducation == CandeID).Select(x => x).FirstOrDefault();
                    CandidateID = int.Parse(mtCandidate.Text);
                    Institute = MtInstitute.Text;
                    Career = mtCareer.Text;
                    Level = MtLevel.SelectedItem.ToString();
                    DateBeg = DateTime.Parse(MtDateBeg.Text);
                    DateEnd = DateTime.Parse(MtDateEnd.Text);
                    if(DateBeg > DateEnd)
                    {

                        MessageBox.Show("DateBeg cannot be greater than DateEnd.");
                        return;
                    }


                    if (mtCandidate.Text == "" || mtCareer.Text == "" || MtInstitute.Text == "")
                    {

                        MessageBox.Show("Please fill all the textbox");
                        return;

                    }

                    CanweU.CandidateID = CandidateID;
                    CanweU.Institute = Institute;
                    CanweU.Career = Career;
                    CanweU.Level = Level;
                    CanweU.DateBeg = DateBeg;
                    CanweU.DateEnd = DateEnd;
                   
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Guardado Exitosamente");
                }


            }

        }
        public void clear()
        {
            mtCandidate.Text = "";
            mtCareer.Text = "";
            MtInstitute.Text = "";
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidatesEducation frm = new FormCandidatesEducation())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }
        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));

        }
    }
}
