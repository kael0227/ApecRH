﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class CandidatesLanguaguesAddAndEdit : MetroFramework.Forms.MetroForm
    {
        public int Cande;
        public int CandidateID;
        public string Languague;
        public string Level;
        public bool edit = false;
        public CandidatesLanguaguesAddAndEdit()
        {
            InitializeComponent();
        }

        private void CandidatesLanguaguesAddAndEdit_Load(object sender, EventArgs e)
        {
            if (edit == true)
            {
                mtcandwork.Text = Cande.ToString();
                mtCandidate.Text = CandidateID.ToString();
                MtLanguage.SelectedItem = Languague;
                MtLevel.SelectedItem = Level;
            }
            using (Model1 db = new Model1())
            {
                if (!db.Candidates.Any(c => c.Inactive == false))
                {
                    MessageBox.Show("There's no any Candidate please insert one.");
                    FormCandidates frm = new FormCandidates();
                    this.Hide();
                    frm.ShowDialog();
                    this.Dispose();
                }
                var query = from c in db.Candidates
                            where c.Inactive == false
                            select c.CandidateID.ToString();



                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                source.AddRange(query.ToArray());
                this.mtCandidate.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.mtCandidate.AutoCompleteSource = AutoCompleteSource.CustomSource;
                this.mtCandidate.AutoCompleteCustomSource = source;

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (edit == false)
            {
                try
                {
                    Model1 db = new Model1();

                    CandidateID = int.Parse(mtCandidate.Text);
                    Languague = MtLanguage.SelectedItem.ToString();
                    Level = MtLevel.SelectedItem.ToString();


                    //WorkExperience dependency validation
                    if (db.CandidatesLanguages.Any(cdwe => cdwe.CandidateID == CandidateID && cdwe.Languague == Languague
                    ))
                    {
                        MessageBox.Show("Duplicate Record. Pls pick another languague.");
                        return;
                    }
                    CandidatesLanguages cwe = new CandidatesLanguages();
                    if (mtCandidate.Text == "")
                    {
                        MessageBox.Show("Please fill all the textbox");
                        return;
                    }

                    cwe.CandidateID = CandidateID;
                    cwe.Languague = Languague;
                    cwe.Level = Level;






                    db.CandidatesLanguages.Add(cwe);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    clear();



                }
                catch (Exception ex)
                {

                    string errormessage = ex.Message;
                }



            }
            else
            {


                using (Model1 db = new Model1())
                {
                    var CanweU = db.CandidatesLanguages.Where(x => x.CandidateEducation == Cande).Select(x => x).FirstOrDefault();
                    CandidateID = int.Parse(mtCandidate.Text);
                    Languague = MtLanguage.SelectedItem.ToString();
                    Level = MtLevel.SelectedItem.ToString();


                    if (mtCandidate.Text == "")
                    {
                        MessageBox.Show("Please fill all the textbox");
                        return;
                    }


                    CanweU.CandidateID = CandidateID;
                    CanweU.Languague = Languague;
                    CanweU.Level = Level;

                    db.SaveChanges();
                    db.Dispose();

                    MessageBox.Show("Guardado Exitosamente");
                    this.Hide();
                    FormCandidatesLanguages frm = new FormCandidatesLanguages();
                    frm.ShowDialog();
                    this.Dispose();
                }


            }

        }
        public void clear()
        {
            mtCandidate.Text = "";
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidatesLanguages frm = new FormCandidatesLanguages())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }
        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));

        }
    }
}
