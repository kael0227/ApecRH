﻿namespace ApecRH
{
    partial class FormPositions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtGoback = new MetroFramework.Controls.MetroTile();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.mtposfilter = new MetroFramework.Controls.MetroTextBox();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.positionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salMinDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salMaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RiskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Risk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inactiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(23, 63);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 1;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(104, 63);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 2;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(266, 64);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 71);
            this.mtDelete.TabIndex = 3;
            this.mtDelete.Text = "Delete";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(185, 64);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 4;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtGoback
            // 
            this.mtGoback.ActiveControl = null;
            this.mtGoback.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtGoback.Location = new System.Drawing.Point(347, 64);
            this.mtGoback.Name = "mtGoback";
            this.mtGoback.Size = new System.Drawing.Size(75, 71);
            this.mtGoback.TabIndex = 5;
            this.mtGoback.Text = "Go back";
            this.mtGoback.UseCustomBackColor = true;
            this.mtGoback.UseSelectable = true;
            this.mtGoback.Click += new System.EventHandler(this.mtGoback_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.positionIDDataGridViewTextBoxColumn,
            this.positionDataGridViewTextBoxColumn,
            this.salMinDataGridViewTextBoxColumn,
            this.salMaxDataGridViewTextBoxColumn,
            this.RiskID,
            this.Risk,
            this.inactiveDataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.positionsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(24, 140);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(765, 223);
            this.dataGridView1.TabIndex = 6;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(439, 32);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(56, 19);
            this.metroLabel1.TabIndex = 7;
            this.metroLabel1.Text = "Posicion";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(438, 79);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 8;
            this.metroLabel2.Text = "Status";
            // 
            // mtposfilter
            // 
            // 
            // 
            // 
            this.mtposfilter.CustomButton.Image = null;
            this.mtposfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtposfilter.CustomButton.Name = "";
            this.mtposfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtposfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtposfilter.CustomButton.TabIndex = 1;
            this.mtposfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtposfilter.CustomButton.UseSelectable = true;
            this.mtposfilter.CustomButton.Visible = false;
            this.mtposfilter.Lines = new string[0];
            this.mtposfilter.Location = new System.Drawing.Point(501, 32);
            this.mtposfilter.MaxLength = 32767;
            this.mtposfilter.Name = "mtposfilter";
            this.mtposfilter.PasswordChar = '\0';
            this.mtposfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtposfilter.SelectedText = "";
            this.mtposfilter.SelectionLength = 0;
            this.mtposfilter.SelectionStart = 0;
            this.mtposfilter.ShortcutsEnabled = true;
            this.mtposfilter.Size = new System.Drawing.Size(121, 23);
            this.mtposfilter.TabIndex = 9;
            this.mtposfilter.UseSelectable = true;
            this.mtposfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtposfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            " ",
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(501, 79);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 10;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Risk";
            this.dataGridViewTextBoxColumn1.HeaderText = "RiskLevel";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // positionsBindingSource
            // 
            this.positionsBindingSource.DataSource = typeof(ApecRH.Models.Positions);
            // 
            // positionIDDataGridViewTextBoxColumn
            // 
            this.positionIDDataGridViewTextBoxColumn.DataPropertyName = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.HeaderText = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.Name = "positionIDDataGridViewTextBoxColumn";
            this.positionIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "Position";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salMinDataGridViewTextBoxColumn
            // 
            this.salMinDataGridViewTextBoxColumn.DataPropertyName = "SalMin";
            this.salMinDataGridViewTextBoxColumn.HeaderText = "SalMin";
            this.salMinDataGridViewTextBoxColumn.Name = "salMinDataGridViewTextBoxColumn";
            this.salMinDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salMaxDataGridViewTextBoxColumn
            // 
            this.salMaxDataGridViewTextBoxColumn.DataPropertyName = "SalMax";
            this.salMaxDataGridViewTextBoxColumn.HeaderText = "SalMax";
            this.salMaxDataGridViewTextBoxColumn.Name = "salMaxDataGridViewTextBoxColumn";
            this.salMaxDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // RiskID
            // 
            this.RiskID.DataPropertyName = "RiskID";
            this.RiskID.HeaderText = "RiskID";
            this.RiskID.Name = "RiskID";
            this.RiskID.ReadOnly = true;
            this.RiskID.Visible = false;
            // 
            // Risk
            // 
            this.Risk.DataPropertyName = "RiskLevel";
            this.Risk.HeaderText = "RiskLevel";
            this.Risk.Name = "Risk";
            this.Risk.ReadOnly = true;
            // 
            // inactiveDataGridViewCheckBoxColumn
            // 
            this.inactiveDataGridViewCheckBoxColumn.DataPropertyName = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.HeaderText = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.Name = "inactiveDataGridViewCheckBoxColumn";
            this.inactiveDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // FormPositions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 373);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.mtposfilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtGoback);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormPositions";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Positions";
            this.Load += new System.EventHandler(this.FormPositions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile mtSearch;
        private MetroFramework.Controls.MetroTile mtAddP;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtGoback;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource positionsBindingSource;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox mtposfilter;
        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salMinDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salMaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RiskID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Risk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inactiveDataGridViewCheckBoxColumn;
    }
}