// <auto-generated />
namespace ApecRH.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class dbz : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(dbz));
        
        string IMigrationMetadata.Id
        {
            get { return "201803021920578_dbz"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
