namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Departments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "Inactive", c => c.Boolean(nullable: false));
            DropColumn("dbo.Departments", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Departments", "Status", c => c.Int(nullable: false));
            DropColumn("dbo.Departments", "Inactive");
        }
    }
}
