namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Departments2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "Inactive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Employees", "Inactive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Nationalities", "Inactive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Positions", "Inactive", c => c.Boolean(nullable: false));
            DropColumn("dbo.Departments", "Status");
            DropColumn("dbo.Employees", "Status");
            DropColumn("dbo.Nationalities", "Status");
            DropColumn("dbo.Positions", "Status");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Positions", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Nationalities", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Employees", "Status", c => c.Int(nullable: false));
            AddColumn("dbo.Departments", "Status", c => c.Int(nullable: false));
            DropColumn("dbo.Positions", "Inactive");
            DropColumn("dbo.Nationalities", "Inactive");
            DropColumn("dbo.Employees", "Inactive");
            DropColumn("dbo.Departments", "Inactive");
        }
    }
}
