namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class com1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CandidatesEducations", "Career", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CandidatesEducations", "Career");
        }
    }
}
