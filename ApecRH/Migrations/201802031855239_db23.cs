namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class db23 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Candidates", "Approved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Candidates", "Approved");
        }
    }
}
