namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDatabase : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Employees", "Department_DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.Employees", "Position_PositionID", "dbo.Positions");
            DropForeignKey("dbo.Positions", "Risk_ID", "dbo.Risks");
            DropIndex("dbo.Employees", new[] { "Department_DepartmentID" });
            DropIndex("dbo.Employees", new[] { "Position_PositionID" });
            DropIndex("dbo.Positions", new[] { "Risk_ID" });
            RenameColumn(table: "dbo.Employees", name: "Department_DepartmentID", newName: "DepartmentRefID");
            RenameColumn(table: "dbo.Employees", name: "Nationality_NationalitiesID", newName: "NationalitiesID");
            RenameColumn(table: "dbo.Employees", name: "Position_PositionID", newName: "PositionID");
            RenameColumn(table: "dbo.Positions", name: "Risk_ID", newName: "RiskID");
            RenameIndex(table: "dbo.Employees", name: "IX_Nationality_NationalitiesID", newName: "IX_NationalitiesID");
            AlterColumn("dbo.Employees", "NationalID", c => c.Int(nullable: false));
            AlterColumn("dbo.Employees", "DepartmentRefID", c => c.Int(nullable: false));
            AlterColumn("dbo.Employees", "PositionID", c => c.Int(nullable: false));
            AlterColumn("dbo.Positions", "RiskID", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "NationalID", unique: true);
            CreateIndex("dbo.Employees", "DepartmentRefID");
            CreateIndex("dbo.Employees", "PositionID");
            CreateIndex("dbo.Positions", "RiskID");
            AddForeignKey("dbo.Employees", "DepartmentRefID", "dbo.Departments", "DepartmentID", cascadeDelete: true);
            AddForeignKey("dbo.Employees", "PositionID", "dbo.Positions", "PositionID", cascadeDelete: true);
            AddForeignKey("dbo.Positions", "RiskID", "dbo.Risks", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Positions", "RiskID", "dbo.Risks");
            DropForeignKey("dbo.Employees", "PositionID", "dbo.Positions");
            DropForeignKey("dbo.Employees", "DepartmentRefID", "dbo.Departments");
            DropIndex("dbo.Positions", new[] { "RiskID" });
            DropIndex("dbo.Employees", new[] { "PositionID" });
            DropIndex("dbo.Employees", new[] { "DepartmentRefID" });
            DropIndex("dbo.Employees", new[] { "NationalID" });
            AlterColumn("dbo.Positions", "RiskID", c => c.Int());
            AlterColumn("dbo.Employees", "PositionID", c => c.Int());
            AlterColumn("dbo.Employees", "DepartmentRefID", c => c.Int());
            AlterColumn("dbo.Employees", "NationalID", c => c.String(maxLength: 255));
            RenameIndex(table: "dbo.Employees", name: "IX_NationalitiesID", newName: "IX_Nationality_NationalitiesID");
            RenameColumn(table: "dbo.Positions", name: "RiskID", newName: "Risk_ID");
            RenameColumn(table: "dbo.Employees", name: "PositionID", newName: "Position_PositionID");
            RenameColumn(table: "dbo.Employees", name: "NationalitiesID", newName: "Nationality_NationalitiesID");
            RenameColumn(table: "dbo.Employees", name: "DepartmentRefID", newName: "Department_DepartmentID");
            CreateIndex("dbo.Positions", "Risk_ID");
            CreateIndex("dbo.Employees", "Position_PositionID");
            CreateIndex("dbo.Employees", "Department_DepartmentID");
            AddForeignKey("dbo.Positions", "Risk_ID", "dbo.Risks", "ID");
            AddForeignKey("dbo.Employees", "Position_PositionID", "dbo.Positions", "PositionID");
            AddForeignKey("dbo.Employees", "Department_DepartmentID", "dbo.Departments", "DepartmentID");
        }
    }
}
