namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Risks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Risks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RiskLevel = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Positions", "Risk_ID", c => c.Int());
            CreateIndex("dbo.Positions", "Risk_ID");
            AddForeignKey("dbo.Positions", "Risk_ID", "dbo.Risks", "ID");
            DropColumn("dbo.Positions", "Risk");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Positions", "Risk", c => c.String(maxLength: 255));
            DropForeignKey("dbo.Positions", "Risk_ID", "dbo.Risks");
            DropIndex("dbo.Positions", new[] { "Risk_ID" });
            DropColumn("dbo.Positions", "Risk_ID");
            DropTable("dbo.Risks");
        }
    }
}
