namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastupdate : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Candidates", new[] { "NationalID" });
            DropIndex("dbo.Employees", new[] { "NationalID" });
            AlterColumn("dbo.Candidates", "NationalID", c => c.String());
            AlterColumn("dbo.Employees", "NationalID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "NationalID", c => c.Int(nullable: false));
            AlterColumn("dbo.Candidates", "NationalID", c => c.Int(nullable: false));
            CreateIndex("dbo.Employees", "NationalID", unique: true);
            CreateIndex("dbo.Candidates", "NationalID", unique: true);
        }
    }
}
