namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CandidatesEducations",
                c => new
                    {
                        CandidateEducation = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Institute = c.String(maxLength: 255),
                        Level = c.String(),
                        DateBeg = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CandidateEducation)
                .ForeignKey("dbo.Candidates", t => t.CandidateID, cascadeDelete: true)
                .Index(t => t.CandidateID);
            
            CreateTable(
                "dbo.CandidatesLanguages",
                c => new
                    {
                        CandidateEducation = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Languague = c.String(maxLength: 255),
                        Level = c.String(),
                    })
                .PrimaryKey(t => t.CandidateEducation)
                .ForeignKey("dbo.Candidates", t => t.CandidateID, cascadeDelete: true)
                .Index(t => t.CandidateID);
            
            CreateTable(
                "dbo.CandidatesSkills",
                c => new
                    {
                        CandidateEducation = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Skill = c.String(maxLength: 255),
                        Level = c.String(),
                    })
                .PrimaryKey(t => t.CandidateEducation)
                .ForeignKey("dbo.Candidates", t => t.CandidateID, cascadeDelete: true)
                .Index(t => t.CandidateID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CandidatesSkills", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidatesLanguages", "CandidateID", "dbo.Candidates");
            DropForeignKey("dbo.CandidatesEducations", "CandidateID", "dbo.Candidates");
            DropIndex("dbo.CandidatesSkills", new[] { "CandidateID" });
            DropIndex("dbo.CandidatesLanguages", new[] { "CandidateID" });
            DropIndex("dbo.CandidatesEducations", new[] { "CandidateID" });
            DropTable("dbo.CandidatesSkills");
            DropTable("dbo.CandidatesLanguages");
            DropTable("dbo.CandidatesEducations");
        }
    }
}
