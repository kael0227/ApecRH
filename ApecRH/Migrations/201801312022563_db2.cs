namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class db2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Candidates",
                c => new
                    {
                        CandidateID = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        LastName = c.String(maxLength: 255),
                        NationalID = c.Int(nullable: false),
                        HireDate = c.DateTime(nullable: false),
                        DepartmentID = c.Int(nullable: false),
                        PositionID = c.Int(nullable: false),
                        NationalityID = c.String(maxLength: 128),
                        Salary = c.Double(nullable: false),
                        Inactive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CandidateID)
                .ForeignKey("dbo.Departments", t => t.DepartmentID, cascadeDelete: true)
                .ForeignKey("dbo.Nationalities", t => t.NationalityID)
                .ForeignKey("dbo.Positions", t => t.PositionID, cascadeDelete: true)
                .Index(t => t.NationalID, unique: true)
                .Index(t => t.DepartmentID)
                .Index(t => t.PositionID)
                .Index(t => t.NationalityID);
            
            CreateTable(
                "dbo.CandidatesWorkExperiences",
                c => new
                    {
                        CandidateWorkExperience = c.Int(nullable: false, identity: true),
                        CandidateID = c.Int(nullable: false),
                        Organization = c.String(maxLength: 255),
                        PositionID = c.Int(nullable: false),
                        DateBeg = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        Salary = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.CandidateWorkExperience)
                .ForeignKey("dbo.Candidates", t => t.CandidateID, cascadeDelete: true)
                .ForeignKey("dbo.Positions", t => t.PositionID, cascadeDelete: false)
                .Index(t => t.CandidateID)
                .Index(t => t.PositionID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Candidates", "PositionID", "dbo.Positions");
            DropForeignKey("dbo.Candidates", "NationalityID", "dbo.Nationalities");
            DropForeignKey("dbo.Candidates", "DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.CandidatesWorkExperiences", "PositionID", "dbo.Positions");
            DropForeignKey("dbo.CandidatesWorkExperiences", "CandidateID", "dbo.Candidates");
            DropIndex("dbo.CandidatesWorkExperiences", new[] { "PositionID" });
            DropIndex("dbo.CandidatesWorkExperiences", new[] { "CandidateID" });
            DropIndex("dbo.Candidates", new[] { "NationalityID" });
            DropIndex("dbo.Candidates", new[] { "PositionID" });
            DropIndex("dbo.Candidates", new[] { "DepartmentID" });
            DropIndex("dbo.Candidates", new[] { "NationalID" });
            DropTable("dbo.CandidatesWorkExperiences");
            DropTable("dbo.Candidates");
        }
    }
}
