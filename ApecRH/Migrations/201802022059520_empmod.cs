namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class empmod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Nationalities", "NationName", c => c.String(maxLength: 255));
            DropColumn("dbo.Nationalities", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Nationalities", "Name", c => c.String(maxLength: 255));
            DropColumn("dbo.Nationalities", "NationName");
        }
    }
}
