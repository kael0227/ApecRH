namespace ApecRH.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class com : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.CandidatesLanguages", name: "CandidateEducation", newName: "CandidatesLangueages");
            RenameColumn(table: "dbo.CandidatesSkills", name: "CandidateEducation", newName: "CandidatesSkills");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.CandidatesSkills", name: "CandidatesSkills", newName: "CandidateEducation");
            RenameColumn(table: "dbo.CandidatesLanguages", name: "CandidatesLangueages", newName: "CandidateEducation");
        }
    }
}
