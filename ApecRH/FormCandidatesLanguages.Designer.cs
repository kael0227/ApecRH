﻿namespace ApecRH
{
    partial class FormCandidatesLanguages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mtcanfilter = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CandidateEducation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtGoBack = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.candidateIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.languagueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.levelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidatesLanguagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesLanguagesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mtcanfilter
            // 
            // 
            // 
            // 
            this.mtcanfilter.CustomButton.Image = null;
            this.mtcanfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtcanfilter.CustomButton.Name = "";
            this.mtcanfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcanfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcanfilter.CustomButton.TabIndex = 1;
            this.mtcanfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcanfilter.CustomButton.UseSelectable = true;
            this.mtcanfilter.CustomButton.Visible = false;
            this.mtcanfilter.Lines = new string[0];
            this.mtcanfilter.Location = new System.Drawing.Point(507, 42);
            this.mtcanfilter.MaxLength = 32767;
            this.mtcanfilter.Name = "mtcanfilter";
            this.mtcanfilter.PasswordChar = '\0';
            this.mtcanfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcanfilter.SelectedText = "";
            this.mtcanfilter.SelectionLength = 0;
            this.mtcanfilter.SelectionStart = 0;
            this.mtcanfilter.ShortcutsEnabled = true;
            this.mtcanfilter.Size = new System.Drawing.Size(121, 23);
            this.mtcanfilter.TabIndex = 39;
            this.mtcanfilter.UseSelectable = true;
            this.mtcanfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcanfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(432, 42);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(69, 19);
            this.metroLabel1.TabIndex = 37;
            this.metroLabel1.Text = "Candidate";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CandidateEducation,
            this.candidateIDDataGridViewTextBoxColumn,
            this.candidateDataGridViewTextBoxColumn,
            this.languagueDataGridViewTextBoxColumn,
            this.levelDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.candidatesLanguagesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(16, 165);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(778, 350);
            this.dataGridView1.TabIndex = 36;
            // 
            // CandidateEducation
            // 
            this.CandidateEducation.DataPropertyName = "CandidateEducation";
            this.CandidateEducation.HeaderText = "CandidateEducation";
            this.CandidateEducation.Name = "CandidateEducation";
            this.CandidateEducation.ReadOnly = true;
            this.CandidateEducation.Visible = false;
            // 
            // mtGoBack
            // 
            this.mtGoBack.ActiveControl = null;
            this.mtGoBack.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtGoBack.Location = new System.Drawing.Point(340, 89);
            this.mtGoBack.Name = "mtGoBack";
            this.mtGoBack.Size = new System.Drawing.Size(75, 70);
            this.mtGoBack.TabIndex = 35;
            this.mtGoBack.Text = "Goback";
            this.mtGoBack.UseCustomBackColor = true;
            this.mtGoBack.UseSelectable = true;
            this.mtGoBack.Click += new System.EventHandler(this.mtGoBack_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(178, 89);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 34;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(259, 89);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 70);
            this.mtDelete.TabIndex = 33;
            this.mtDelete.Text = "Delete";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(97, 89);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 32;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(16, 89);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 31;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.mtSearch_Click);
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(507, 104);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 41;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(432, 104);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 40;
            this.metroLabel2.Text = "Status";
            // 
            // candidateIDDataGridViewTextBoxColumn
            // 
            this.candidateIDDataGridViewTextBoxColumn.DataPropertyName = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.HeaderText = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.Name = "candidateIDDataGridViewTextBoxColumn";
            this.candidateIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // candidateDataGridViewTextBoxColumn
            // 
            this.candidateDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.candidateDataGridViewTextBoxColumn.DataPropertyName = "Candidate";
            this.candidateDataGridViewTextBoxColumn.HeaderText = "Candidate";
            this.candidateDataGridViewTextBoxColumn.Name = "candidateDataGridViewTextBoxColumn";
            this.candidateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // languagueDataGridViewTextBoxColumn
            // 
            this.languagueDataGridViewTextBoxColumn.DataPropertyName = "Languague";
            this.languagueDataGridViewTextBoxColumn.HeaderText = "Languague";
            this.languagueDataGridViewTextBoxColumn.Name = "languagueDataGridViewTextBoxColumn";
            this.languagueDataGridViewTextBoxColumn.ReadOnly = true;
            this.languagueDataGridViewTextBoxColumn.Width = 150;
            // 
            // levelDataGridViewTextBoxColumn
            // 
            this.levelDataGridViewTextBoxColumn.DataPropertyName = "Level";
            this.levelDataGridViewTextBoxColumn.HeaderText = "Level";
            this.levelDataGridViewTextBoxColumn.Name = "levelDataGridViewTextBoxColumn";
            this.levelDataGridViewTextBoxColumn.ReadOnly = true;
            this.levelDataGridViewTextBoxColumn.Width = 150;
            // 
            // candidatesLanguagesBindingSource
            // 
            this.candidatesLanguagesBindingSource.DataSource = typeof(ApecRH.Models.CandidatesLanguages);
            // 
            // FormCandidatesLanguages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 572);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.mtcanfilter);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtGoBack);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormCandidatesLanguages";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Languages";
            this.Load += new System.EventHandler(this.FormCandidatesLanguages_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesLanguagesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox mtcanfilter;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource candidatesLanguagesBindingSource;
        private MetroFramework.Controls.MetroTile mtGoBack;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtAddP;
        private MetroFramework.Controls.MetroTile mtSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn CandidateEducation;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn languagueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn levelDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}