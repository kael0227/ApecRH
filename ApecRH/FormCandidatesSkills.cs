﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class FormCandidatesSkills : MetroFramework.Forms.MetroForm
    {
        public FormCandidatesSkills()
        {
            InitializeComponent();
        }

        private void FormCandidatesSkills_Load(object sender, EventArgs e)
        {
            mcskillfilter.SelectedItem = "Active";
            mcskillfilter.Items.Add("All");
            mcskillfilter.DataSource = Enum.GetNames(typeof(Skills));
           

            using (Model1 db = new Model1())
            {
                var result = from cs in db.CandidatesSkills
                             join c in db.Candidates on cs.CandidateID equals c.CandidateID
                             where c.Inactive == false
                             select new
                             {
                                 cs.CandidateEducation,
                                 cs.CandidateID,
                                 Name = c.Name + " " + c.LastName,
                                 cs.Skill,
                                 cs.Level,
                             };
                dataGridView1.DataSource = result.ToList();
            }

        }

        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (CandidatesSkillsAddAndEdit frm = new CandidatesSkillsAddAndEdit())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtEdit_Click(object sender, EventArgs e)
        {
            using (CandidatesSkillsAddAndEdit frm = new CandidatesSkillsAddAndEdit())
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Please Select a row");
                    return;
                }
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                frm.Cande = int.Parse(row.Cells[0].Value.ToString());
                frm.CandidateID = int.Parse(row.Cells[1].Value.ToString());
                frm.Skill = row.Cells[3].Value.ToString();
                frm.Level = row.Cells[4].Value.ToString();
                frm.edit = true;
                this.Hide();
                frm.ShowDialog();
                this.Dispose();

            }
        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to delete?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {

                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {
                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        var CandSD = db.CandidatesSkills.First(c => c.CandidateEducation == toBeDeleted);
                        var notToBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[1].Value;
                        bool inactiveD = (from d in db.Candidates
                                          where d.CandidateID == notToBeDeleted
                                          select d.Inactive).Single();
                        if (inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar datos Inactivos. Contacte al supervisor");
                            return;
                        }
                        db.CandidatesSkills.Remove(CandSD);
                        db.SaveChanges();
                        var result = from cl in db.CandidatesSkills
                                     join c in db.Candidates on cl.CandidateID equals c.CandidateID
                                     where c.Inactive == false
                                     select new
                                     {
                                         cl.CandidateEducation,
                                         cl.CandidateID,
                                         Candidate = c.Name + " " + c.LastName,
                                         cl.Skill,
                                         cl.Level,
                                         c.Inactive
                                     };
                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void mtSearch_Click(object sender, EventArgs e)
        {
            Model1 db = new Model1();

            var result = from cs in db.CandidatesSkills
                         join c in db.Candidates on cs.CandidateID equals c.CandidateID
                         select new
                         {
                             cs.CandidateEducation,
                             cs.CandidateID,
                             Name = c.Name + " " + c.LastName,
                             cs.Skill,
                             cs.Level,
                             c.Inactive,
                         };

            if (mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(nw => nw.Inactive == false);
                dataGridView1.DataSource = result.ToList();
            }
            else if (mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(nw => nw.Inactive == true);
                dataGridView1.DataSource = result.ToList();
            }

            if (mcskillfilter.SelectedItem.ToString() == "All" )
            {
                dataGridView1.DataSource = result.ToList();
            }
            else
            {
                result = result.Where(s => s.Skill == mcskillfilter.SelectedItem.ToString());
            }

            if (mtcanfilter.Text != "")
            {

                result = result.Where(c => c.CandidateID.ToString().Contains(mtcanfilter.Text) ||
                                  c.Name.Contains(mtcanfilter.Text));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtcanfilter.Text == "")
            {
                dataGridView1.DataSource = result.ToList();
            }
        }

        private void mtGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuRecruiment frm = new MenuRecruiment();
            frm.ShowDialog();
            this.Dispose();
        }
    }
}