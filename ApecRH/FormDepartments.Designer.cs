﻿namespace ApecRH
{
    partial class FormDepartments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnSearch = new MetroFramework.Controls.MetroTile();
            this.mtAdd = new MetroFramework.Controls.MetroTile();
            this.MtEdit = new MetroFramework.Controls.MetroTile();
            this.metroTile4 = new MetroFramework.Controls.MetroTile();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.mcincativefiler = new MetroFramework.Controls.MetroComboBox();
            this.mtfilterdp = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mtGoback = new MetroFramework.Controls.MetroTile();
            this.bindingSourceDeparments = new System.Windows.Forms.BindingSource(this.components);
            this.departmentIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.departmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDeparments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.ActiveControl = null;
            this.btnSearch.BackColor = System.Drawing.Color.Navy;
            this.btnSearch.Location = new System.Drawing.Point(23, 63);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 70);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseCustomBackColor = true;
            this.btnSearch.UseSelectable = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // mtAdd
            // 
            this.mtAdd.ActiveControl = null;
            this.mtAdd.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAdd.Location = new System.Drawing.Point(104, 63);
            this.mtAdd.Name = "mtAdd";
            this.mtAdd.Size = new System.Drawing.Size(75, 70);
            this.mtAdd.TabIndex = 1;
            this.mtAdd.Text = "Add";
            this.mtAdd.UseCustomBackColor = true;
            this.mtAdd.UseSelectable = true;
            this.mtAdd.Click += new System.EventHandler(this.mtAdd_Click);
            // 
            // MtEdit
            // 
            this.MtEdit.ActiveControl = null;
            this.MtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.MtEdit.Location = new System.Drawing.Point(185, 63);
            this.MtEdit.Name = "MtEdit";
            this.MtEdit.Size = new System.Drawing.Size(75, 70);
            this.MtEdit.TabIndex = 2;
            this.MtEdit.Text = "Edit";
            this.MtEdit.UseCustomBackColor = true;
            this.MtEdit.UseSelectable = true;
            this.MtEdit.Click += new System.EventHandler(this.MtEdit_Click);
            // 
            // metroTile4
            // 
            this.metroTile4.ActiveControl = null;
            this.metroTile4.BackColor = System.Drawing.Color.DarkRed;
            this.metroTile4.Location = new System.Drawing.Point(266, 63);
            this.metroTile4.Name = "metroTile4";
            this.metroTile4.Size = new System.Drawing.Size(75, 70);
            this.metroTile4.TabIndex = 3;
            this.metroTile4.Text = "Delete";
            this.metroTile4.UseCustomBackColor = true;
            this.metroTile4.UseSelectable = true;
            this.metroTile4.Click += new System.EventHandler(this.metroTile4_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.departmentIDDataGridViewTextBoxColumn,
            this.departmentDataGridViewTextBoxColumn,
            this.dataGridViewCheckBoxColumn1});
            this.dataGridView1.DataSource = this.departmentsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(24, 140);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(593, 287);
            this.dataGridView1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(459, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Departamento";
            // 
            // mcincativefiler
            // 
            this.mcincativefiler.FormattingEnabled = true;
            this.mcincativefiler.ItemHeight = 23;
            this.mcincativefiler.Items.AddRange(new object[] {
            " ",
            "Inactive",
            "Active"});
            this.mcincativefiler.Location = new System.Drawing.Point(600, 102);
            this.mcincativefiler.Name = "mcincativefiler";
            this.mcincativefiler.Size = new System.Drawing.Size(121, 29);
            this.mcincativefiler.TabIndex = 7;
            this.mcincativefiler.UseSelectable = true;
            // 
            // mtfilterdp
            // 
            // 
            // 
            // 
            this.mtfilterdp.CustomButton.Image = null;
            this.mtfilterdp.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtfilterdp.CustomButton.Name = "";
            this.mtfilterdp.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtfilterdp.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtfilterdp.CustomButton.TabIndex = 1;
            this.mtfilterdp.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtfilterdp.CustomButton.UseSelectable = true;
            this.mtfilterdp.CustomButton.Visible = false;
            this.mtfilterdp.Lines = new string[0];
            this.mtfilterdp.Location = new System.Drawing.Point(600, 47);
            this.mtfilterdp.MaxLength = 32767;
            this.mtfilterdp.Name = "mtfilterdp";
            this.mtfilterdp.PasswordChar = '\0';
            this.mtfilterdp.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtfilterdp.SelectedText = "";
            this.mtfilterdp.SelectionLength = 0;
            this.mtfilterdp.SelectionStart = 0;
            this.mtfilterdp.ShortcutsEnabled = true;
            this.mtfilterdp.Size = new System.Drawing.Size(121, 23);
            this.mtfilterdp.TabIndex = 8;
            this.mtfilterdp.UseSelectable = true;
            this.mtfilterdp.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtfilterdp.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(459, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Status";
            // 
            // mtGoback
            // 
            this.mtGoback.ActiveControl = null;
            this.mtGoback.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtGoback.Location = new System.Drawing.Point(347, 63);
            this.mtGoback.Name = "mtGoback";
            this.mtGoback.Size = new System.Drawing.Size(81, 71);
            this.mtGoback.TabIndex = 10;
            this.mtGoback.Text = "Go Back";
            this.mtGoback.UseCustomBackColor = true;
            this.mtGoback.UseSelectable = true;
            this.mtGoback.Click += new System.EventHandler(this.mtGoback_Click);
            // 
            // departmentIDDataGridViewTextBoxColumn
            // 
            this.departmentIDDataGridViewTextBoxColumn.DataPropertyName = "DepartmentID";
            this.departmentIDDataGridViewTextBoxColumn.HeaderText = "DepartmentID";
            this.departmentIDDataGridViewTextBoxColumn.Name = "departmentIDDataGridViewTextBoxColumn";
            this.departmentIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departmentDataGridViewTextBoxColumn
            // 
            this.departmentDataGridViewTextBoxColumn.DataPropertyName = "Department";
            this.departmentDataGridViewTextBoxColumn.HeaderText = "Department";
            this.departmentDataGridViewTextBoxColumn.Name = "departmentDataGridViewTextBoxColumn";
            this.departmentDataGridViewTextBoxColumn.ReadOnly = true;
            this.departmentDataGridViewTextBoxColumn.Width = 300;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "Inactive";
            this.dataGridViewCheckBoxColumn1.HeaderText = "Inactive";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Width = 150;
            // 
            // departmentsBindingSource
            // 
            this.departmentsBindingSource.DataSource = typeof(ApecRH.Models.Departments);
            // 
            // FormDepartments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 450);
            this.Controls.Add(this.mtGoback);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mtfilterdp);
            this.Controls.Add(this.mcincativefiler);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.metroTile4);
            this.Controls.Add(this.MtEdit);
            this.Controls.Add(this.mtAdd);
            this.Controls.Add(this.btnSearch);
            this.Name = "FormDepartments";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Departments";
            this.Load += new System.EventHandler(this.Departments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDeparments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departmentsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTile btnSearch;
        private MetroFramework.Controls.MetroTile mtAdd;
        private MetroFramework.Controls.MetroTile MtEdit;
        private MetroFramework.Controls.MetroTile metroTile4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource departmentsBindingSource;
        private System.Windows.Forms.BindingSource bindingSourceDeparments;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox mcincativefiler;
        private MetroFramework.Controls.MetroTextBox mtfilterdp;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroTile mtGoback;
    }
}

