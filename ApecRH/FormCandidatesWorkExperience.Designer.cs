﻿namespace ApecRH
{
    partial class FormCandidatesWorkExperience
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.mtcanfilter = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CandidateWorkExperience = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Candidate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtGoBack = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.candidateIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.organizationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateBegDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateEndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.candidatesWorkExperienceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesWorkExperienceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(535, 119);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 20;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // mtcanfilter
            // 
            // 
            // 
            // 
            this.mtcanfilter.CustomButton.Image = null;
            this.mtcanfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtcanfilter.CustomButton.Name = "";
            this.mtcanfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcanfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcanfilter.CustomButton.TabIndex = 1;
            this.mtcanfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcanfilter.CustomButton.UseSelectable = true;
            this.mtcanfilter.CustomButton.Visible = false;
            this.mtcanfilter.Lines = new string[0];
            this.mtcanfilter.Location = new System.Drawing.Point(535, 72);
            this.mtcanfilter.MaxLength = 32767;
            this.mtcanfilter.Name = "mtcanfilter";
            this.mtcanfilter.PasswordChar = '\0';
            this.mtcanfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcanfilter.SelectedText = "";
            this.mtcanfilter.SelectionLength = 0;
            this.mtcanfilter.SelectionStart = 0;
            this.mtcanfilter.ShortcutsEnabled = true;
            this.mtcanfilter.Size = new System.Drawing.Size(121, 23);
            this.mtcanfilter.TabIndex = 19;
            this.mtcanfilter.UseSelectable = true;
            this.mtcanfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcanfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(454, 119);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 18;
            this.metroLabel2.Text = "Status";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(455, 72);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(74, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Candidates";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CandidateWorkExperience,
            this.candidateIDDataGridViewTextBoxColumn,
            this.Candidate,
            this.organizationDataGridViewTextBoxColumn,
            this.positionIDDataGridViewTextBoxColumn,
            this.positionDataGridViewTextBoxColumn,
            this.dateBegDataGridViewTextBoxColumn,
            this.dateEndDataGridViewTextBoxColumn,
            this.salaryDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.candidatesWorkExperienceBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(5, 180);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(846, 281);
            this.dataGridView1.TabIndex = 16;
            // 
            // CandidateWorkExperience
            // 
            this.CandidateWorkExperience.DataPropertyName = "CandidateWorkExperience";
            this.CandidateWorkExperience.HeaderText = "CandidateWorkExperience";
            this.CandidateWorkExperience.Name = "CandidateWorkExperience";
            this.CandidateWorkExperience.ReadOnly = true;
            this.CandidateWorkExperience.Visible = false;
            // 
            // Candidate
            // 
            this.Candidate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Candidate.DataPropertyName = "Name";
            this.Candidate.HeaderText = "Candidate";
            this.Candidate.Name = "Candidate";
            this.Candidate.ReadOnly = true;
            // 
            // mtGoBack
            // 
            this.mtGoBack.ActiveControl = null;
            this.mtGoBack.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtGoBack.Location = new System.Drawing.Point(363, 104);
            this.mtGoBack.Name = "mtGoBack";
            this.mtGoBack.Size = new System.Drawing.Size(75, 70);
            this.mtGoBack.TabIndex = 15;
            this.mtGoBack.Text = "Go Back";
            this.mtGoBack.UseCustomBackColor = true;
            this.mtGoBack.UseSelectable = true;
            this.mtGoBack.Click += new System.EventHandler(this.mtGoBack_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(201, 104);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 14;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(282, 104);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 70);
            this.mtDelete.TabIndex = 13;
            this.mtDelete.Text = "Delete";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(120, 103);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 12;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(39, 103);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 11;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.mtSearch_Click);
            // 
            // candidateIDDataGridViewTextBoxColumn
            // 
            this.candidateIDDataGridViewTextBoxColumn.DataPropertyName = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.HeaderText = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.Name = "candidateIDDataGridViewTextBoxColumn";
            this.candidateIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // organizationDataGridViewTextBoxColumn
            // 
            this.organizationDataGridViewTextBoxColumn.DataPropertyName = "Organization";
            this.organizationDataGridViewTextBoxColumn.HeaderText = "Organization";
            this.organizationDataGridViewTextBoxColumn.Name = "organizationDataGridViewTextBoxColumn";
            this.organizationDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // positionIDDataGridViewTextBoxColumn
            // 
            this.positionIDDataGridViewTextBoxColumn.DataPropertyName = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.HeaderText = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.Name = "positionIDDataGridViewTextBoxColumn";
            this.positionIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.positionIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "Position";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateBegDataGridViewTextBoxColumn
            // 
            this.dateBegDataGridViewTextBoxColumn.DataPropertyName = "DateBeg";
            this.dateBegDataGridViewTextBoxColumn.HeaderText = "DateBeg";
            this.dateBegDataGridViewTextBoxColumn.Name = "dateBegDataGridViewTextBoxColumn";
            this.dateBegDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateEndDataGridViewTextBoxColumn
            // 
            this.dateEndDataGridViewTextBoxColumn.DataPropertyName = "DateEnd";
            this.dateEndDataGridViewTextBoxColumn.HeaderText = "DateEnd";
            this.dateEndDataGridViewTextBoxColumn.Name = "dateEndDataGridViewTextBoxColumn";
            this.dateEndDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salaryDataGridViewTextBoxColumn
            // 
            this.salaryDataGridViewTextBoxColumn.DataPropertyName = "Salary";
            this.salaryDataGridViewTextBoxColumn.HeaderText = "Salary";
            this.salaryDataGridViewTextBoxColumn.Name = "salaryDataGridViewTextBoxColumn";
            this.salaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // candidatesWorkExperienceBindingSource
            // 
            this.candidatesWorkExperienceBindingSource.DataSource = typeof(ApecRH.Models.CandidatesWorkExperience);
            // 
            // FormCandidatesWorkExperience
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 469);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.mtcanfilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtGoBack);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormCandidatesWorkExperience";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "WorkExperience";
            this.Load += new System.EventHandler(this.FormCandidatesWorkExperience_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesWorkExperienceBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private MetroFramework.Controls.MetroTextBox mtcanfilter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource candidatesWorkExperienceBindingSource;
        private MetroFramework.Controls.MetroTile mtGoBack;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtAddP;
        private MetroFramework.Controls.MetroTile mtSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn CandidateWorkExperience;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Candidate;
        private System.Windows.Forms.DataGridViewTextBoxColumn organizationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateBegDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateEndDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryDataGridViewTextBoxColumn;
    }
}