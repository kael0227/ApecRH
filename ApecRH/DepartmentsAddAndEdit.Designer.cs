﻿namespace ApecRH
{
    partial class DepartmentsAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.mtxtDepID = new MetroFramework.Controls.MetroTextBox();
            this.bindingSourceDepartments = new System.Windows.Forms.BindingSource(this.components);
            this.mtDep = new MetroFramework.Controls.MetroTextBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.MtStatus = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.btnSaveAndQuit = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDepartments)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(15, 40);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(92, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "DepartmentID";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(15, 84);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(116, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "DepartmentName";
            // 
            // mtxtDepID
            // 
            // 
            // 
            // 
            this.mtxtDepID.CustomButton.Image = null;
            this.mtxtDepID.CustomButton.Location = new System.Drawing.Point(167, 1);
            this.mtxtDepID.CustomButton.Name = "";
            this.mtxtDepID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtxtDepID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtxtDepID.CustomButton.TabIndex = 1;
            this.mtxtDepID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtxtDepID.CustomButton.UseSelectable = true;
            this.mtxtDepID.CustomButton.Visible = false;
            this.mtxtDepID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceDepartments, "DepartmentID", true));
            this.mtxtDepID.Lines = new string[0];
            this.mtxtDepID.Location = new System.Drawing.Point(164, 36);
            this.mtxtDepID.MaxLength = 4;
            this.mtxtDepID.Name = "mtxtDepID";
            this.mtxtDepID.PasswordChar = '\0';
            this.mtxtDepID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtxtDepID.SelectedText = "";
            this.mtxtDepID.SelectionLength = 0;
            this.mtxtDepID.SelectionStart = 0;
            this.mtxtDepID.ShortcutsEnabled = true;
            this.mtxtDepID.Size = new System.Drawing.Size(189, 23);
            this.mtxtDepID.TabIndex = 2;
            this.mtxtDepID.UseSelectable = true;
            this.mtxtDepID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtxtDepID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtxtDepID.Click += new System.EventHandler(this.metroTextBox1_Click);
            this.mtxtDepID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtxtDepID_KeyPress);
            // 
            // bindingSourceDepartments
            // 
            this.bindingSourceDepartments.DataSource = typeof(ApecRH.Models.Departments);
            // 
            // mtDep
            // 
            // 
            // 
            // 
            this.mtDep.CustomButton.Image = null;
            this.mtDep.CustomButton.Location = new System.Drawing.Point(167, 1);
            this.mtDep.CustomButton.Name = "";
            this.mtDep.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtDep.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtDep.CustomButton.TabIndex = 1;
            this.mtDep.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtDep.CustomButton.UseSelectable = true;
            this.mtDep.CustomButton.Visible = false;
            this.mtDep.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSourceDepartments, "Department", true));
            this.mtDep.Lines = new string[0];
            this.mtDep.Location = new System.Drawing.Point(164, 84);
            this.mtDep.MaxLength = 32767;
            this.mtDep.Name = "mtDep";
            this.mtDep.PasswordChar = '\0';
            this.mtDep.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtDep.SelectedText = "";
            this.mtDep.SelectionLength = 0;
            this.mtDep.SelectionStart = 0;
            this.mtDep.ShortcutsEnabled = true;
            this.mtDep.Size = new System.Drawing.Size(189, 23);
            this.mtDep.TabIndex = 3;
            this.mtDep.UseSelectable = true;
            this.mtDep.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtDep.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtDep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(245, 214);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(97, 23);
            this.metroButton1.TabIndex = 4;
            this.metroButton1.Text = "Save";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // MtStatus
            // 
            this.MtStatus.AutoSize = true;
            this.MtStatus.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bindingSourceDepartments, "Inactive", true));
            this.MtStatus.Location = new System.Drawing.Point(164, 127);
            this.MtStatus.Name = "MtStatus";
            this.MtStatus.Size = new System.Drawing.Size(64, 15);
            this.MtStatus.TabIndex = 5;
            this.MtStatus.Text = "Inactive";
            this.MtStatus.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(15, 123);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(52, 19);
            this.metroLabel3.TabIndex = 6;
            this.metroLabel3.Text = "Inactive";
            // 
            // btnSaveAndQuit
            // 
            this.btnSaveAndQuit.Location = new System.Drawing.Point(457, 21);
            this.btnSaveAndQuit.Name = "btnSaveAndQuit";
            this.btnSaveAndQuit.Size = new System.Drawing.Size(86, 23);
            this.btnSaveAndQuit.TabIndex = 7;
            this.btnSaveAndQuit.Text = "Save And Quit";
            this.btnSaveAndQuit.UseSelectable = true;
            this.btnSaveAndQuit.Visible = false;
            this.btnSaveAndQuit.Click += new System.EventHandler(this.btnSaveAndQuit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.mtxtDepID);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroButton1);
            this.groupBox1.Controls.Add(this.MtStatus);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.mtDep);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Location = new System.Drawing.Point(64, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(465, 272);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(120, 214);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 8;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // DepartmentsAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 406);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSaveAndQuit);
            this.Name = "DepartmentsAddAndEdit";
            this.Text = "Departments Info";
            this.Load += new System.EventHandler(this.DepartmentsAddAndEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceDepartments)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        public MetroFramework.Controls.MetroTextBox mtxtDepID;
        private MetroFramework.Controls.MetroTextBox mtDep;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroCheckBox MtStatus;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.BindingSource bindingSourceDepartments;
        public MetroFramework.Controls.MetroButton btnSaveAndQuit;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton BtnCancel;
    }
}