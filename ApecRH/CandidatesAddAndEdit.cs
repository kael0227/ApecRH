﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class CandidatesAddAndEdit : MetroFramework.Forms.MetroForm
    {
        Candidates emp = new Candidates();
        public int empID;
        public string empName;
        public string empLastName;
        public string empNationalID;
        public DateTime empHireDate;
        public int empDep;
        public int empPos;
        public string empNation;
        public double empSalary;
        public bool inactive;
        public bool edit = false;
        public bool empedit = false;
        public CandidatesAddAndEdit()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (edit == false)
            {
                try
                {
                    Model1 db = new Model1();
                    empID = int.Parse(mtEmployee.Text);
                    empName = mtName.Text;
                    empLastName = mtLastName.Text;
                    empNationalID = mtNationalID.Text;
                    if (mtNationalID.Text.Length < 11)
                    {
                        MessageBox.Show("National ID Can't be smaller than 11 numbers");
                        return;
                    }
                    empHireDate = DateTime.Parse(mtDate.Text.ToString());
                    empDep = int.Parse(McDepartment.SelectedValue.ToString());
                    empPos = int.Parse(McPosition.SelectedValue.ToString());
                    empNation = mcNationality.SelectedValue.ToString();
                    empSalary = double.Parse(mtSalary.Text);
                    //salary validation
                    double salValdMax = (from p in db.Positions
                                         where p.PositionID == empPos
                                         select p.SalMax).Sum();

                    double salValdMin = (from p in db.Positions
                                         where p.PositionID == empPos
                                         select p.SalMin).Sum();

                    if (empSalary > salValdMax || empSalary < salValdMin)
                    {
                        MessageBox.Show("Salario tiene que estar entre " + salValdMin + " y " + salValdMax);
                        mtSalary.Focus();
                        return;
                    }
                    inactive = McInactive.Checked;

                    emp.CandidateID = empID;
                    if (db.Candidates.Any(c => c.CandidateID == empID))
                    {
                        MessageBox.Show("Code Exists. Please enter a diferent code");
                        mtEmployee.Focus();
                        return;
                    }
                    emp.Name = empName;
                    emp.LastName = empLastName;
                    emp.NationalID = empNationalID;
                    if (db.Candidates.Any(c => c.NationalID == empNationalID))
                    {
                        MessageBox.Show("NationalID Already Exists.");
                        return;
                    }
                    emp.HireDate = empHireDate;
                    emp.DepartmentID = empDep;
                    emp.PositionID = empPos;
                    emp.NationalityID = empNation;
                    emp.Salary = empSalary;
                    emp.Inactive = inactive;

                    db.Candidates.Add(emp);
                    db.SaveChanges();
                    MessageBox.Show("Saved!");
                    clear();
                    db.Dispose();

                }
                catch (Exception ex)
                {

                    string errormessage = ex.Message;
                    MessageBox.Show(errormessage);
                }

            }
            else if(empedit == true)
            {
                try
                {
                    using (Model1 db = new Model1())
                    {
                        var empU = db.Employees.Where(x => x.EmployeeID == empID).Select(x => x).FirstOrDefault();
                        empName = mtName.Text;
                        empLastName = mtLastName.Text;
                        empNationalID = mtNationalID.Text;
                        empHireDate = DateTime.Parse(mtDate.Text.ToString());
                        empDep = int.Parse(McDepartment.SelectedValue.ToString());
                        empPos = int.Parse(McPosition.SelectedValue.ToString());
                        empNation = mcNationality.SelectedValue.ToString();
                        empSalary = double.Parse(mtSalary.Text);
                        inactive = McInactive.Checked;

                        empU.EmployeeID = empID;
                        empU.Name = empName;
                        empU.LastName = empLastName;
                        empU.NationalID = empNationalID;
                        empU.HireDate = empHireDate;
                        empU.DepartmentRefID = empDep;
                        empU.PositionID = empPos;
                        empU.NationalitiesID = empNation;
                        empU.Salary = empSalary;
                        //salary validation
                        double salValdMax = (from p in db.Positions
                                             where p.PositionID == empPos
                                             select p.SalMax).Sum();

                        double salValdMin = (from p in db.Positions
                                             where p.PositionID == empPos
                                             select p.SalMin).Sum();

                        if (empSalary > salValdMax || empSalary < salValdMin)
                        {
                            MessageBox.Show("Salario tiene que estar entre " + salValdMin + " y " + salValdMax);
                            mtSalary.Focus();
                            return;
                        }
                        empU.Inactive = inactive;



                        db.SaveChanges();
                        db.Dispose();
                        this.Hide();
                        FormEmployee frm2 = new FormEmployee();
                        frm2.ShowDialog();
                        this.Dispose();

                    }
                }
                catch (Exception ex)
                {

                    string errormsg = ex.Message;
                }


            }
            else
            {
                try
                {
                    using (Model1 db = new Model1())
                    {
                        var empU = db.Candidates.Where(x => x.CandidateID == empID).Select(x => x).FirstOrDefault();
                        empName = mtName.Text;
                        empLastName = mtLastName.Text;
                        empNationalID = mtNationalID.Text;
                        empHireDate = DateTime.Parse(mtDate.Text.ToString());
                        empDep = int.Parse(McDepartment.SelectedValue.ToString());
                        empPos = int.Parse(McPosition.SelectedValue.ToString());
                        empNation = mcNationality.SelectedValue.ToString();
                        empSalary = double.Parse(mtSalary.Text);
                        inactive = McInactive.Checked;

                        empU.CandidateID = empID;
                        empU.Name = empName;
                        empU.LastName = empLastName;
                        empU.NationalID = empNationalID;
                        empU.HireDate = empHireDate;
                        empU.DepartmentID = empDep;
                        empU.PositionID = empPos;
                        empU.NationalityID = empNation;
                        empU.Salary = empSalary;
                        //salary validation
                        double salValdMax = (from p in db.Positions
                                             where p.PositionID == empPos
                                             select p.SalMax).Sum();

                        double salValdMin = (from p in db.Positions
                                             where p.PositionID == empPos
                                             select p.SalMin).Sum();

                        if (empSalary > salValdMax || empSalary < salValdMin)
                        {
                            MessageBox.Show("Salario tiene que estar entre " + salValdMin + " y " + salValdMax);
                            mtSalary.Focus();
                            return;
                        }
                        empU.Inactive = inactive;



                        db.SaveChanges();
                        db.Dispose();
                        this.Hide();
                        FormCandidates frm2 = new FormCandidates();
                        frm2.ShowDialog();
                        this.Dispose();

                    }
                }
                catch (Exception ex)
                {

                    string errormsg = ex.Message;
                }

            }

        }


        private void EmployeesAddAndEdit_Load(object sender, EventArgs e)
        {
            
            using (Model1 db = new Model1())
            {
                var result = from d in db.Departments
                             where d.Inactive == false
                             select d;

                McDepartment.DataSource = result.ToList();
                McDepartment.ValueMember = "DepartmentID";
                McDepartment.DisplayMember = "Department";
                if (!db.Departments.Any(d => d.Inactive == false))
                {
                    MessageBox.Show("Please insert a department");
                    this.Hide();
                    using(FormDepartments frm = new FormDepartments())
                    {
                        frm.ShowDialog();
                    }
                    this.Dispose();
                }
            }
            using (Model1 db = new Model1())
            {
                var result = from p in db.Positions
                             where p.Inactive == false
                             select p;
                McPosition.DataSource = result.ToList();
                McPosition.ValueMember = "PositionID";
                McPosition.DisplayMember = "Position";
                if (!db.Positions.Any(d => d.Inactive == false))
                {
                    MessageBox.Show("Please insert a Position");
                    this.Hide();
                    using (FormPositions frm = new FormPositions())
                    {
                        frm.ShowDialog();
                    }
                    this.Dispose();
                }
            }
            using (Model1 db = new Model1())
            {
                var result = from n in db.Nationalities
                             where n.Inactive == false
                             select n;
                mcNationality.DataSource = result.ToList();
                mcNationality.ValueMember = "NationalitiesID";
                mcNationality.DisplayMember = "NationName";
            }

            if (edit == true)
            {
                mtEmployee.Text = empID.ToString();
                mtName.Text = empName;
                mtLastName.Text = empLastName;
                mtNationalID.Text = empNationalID.ToString();
                mtDate.Text = empHireDate.ToString();
                McDepartment.SelectedValue = empDep;
                McPosition.SelectedValue = empPos;
                mcNationality.SelectedItem = empNation;
                mtSalary.Text = empSalary.ToString();
                McInactive.Checked = inactive;
            }

        }

        private void btnsavequit_Click(object sender, EventArgs e)
        {
            try
            {
                Model1 db = new Model1();
                empID = int.Parse(mtEmployee.Text);
                empName = mtName.Text;
                empLastName = mtLastName.Text;
                empNationalID = mtNationalID.Text;
                empHireDate = DateTime.Parse(mtDate.Text.ToString());
                empDep = int.Parse(McDepartment.SelectedValue.ToString());
                empPos = int.Parse(McPosition.SelectedValue.ToString());

                empNation = mcNationality.SelectedValue.ToString();
                //Validation of sal max and min
                empSalary = double.Parse(mtSalary.Text);
                double salValdMax = (from p in db.Positions
                                     where p.PositionID == empPos
                                     select p.SalMax).Sum();

                double salValdMin = (from p in db.Positions
                                     where p.PositionID == empPos
                                     select p.SalMin).Sum();

                if (empSalary > salValdMax || empSalary < salValdMin)
                {
                    MessageBox.Show("Salario tiene que estar entre " + salValdMin + " y " + salValdMax);
                    mtSalary.Focus();
                    return;

                }
                inactive = McInactive.Checked;

                emp.CandidateID = empID;
                //employee validation
                if (db.Candidates.Any(c => c.CandidateID == empID))
                {
                    MessageBox.Show("Code Exists. Please enter a different code.");
                    mtEmployee.Focus();
                    return;
                }
                emp.Name = empName;
                emp.LastName = empLastName;
                emp.NationalID = empNationalID;
                if (db.Candidates.Any(c => c.NationalID == empNationalID))
                {
                    MessageBox.Show("NationalID Already Exists.");
                    return;
                }

                emp.HireDate = empHireDate;
                emp.DepartmentID = empDep;
                emp.PositionID = empPos;
                emp.NationalityID = empNation;
                emp.Salary = empSalary;
                emp.Inactive = inactive;

                db.Candidates.Add(emp);
                db.SaveChanges();
                MessageBox.Show("Guardado exitosamente");
                db.Dispose();


                this.Hide();
                using (FormCandidates frm = new FormCandidates())
                {
                    frm.ShowDialog();
                }
                this.Dispose();
            }
            catch (Exception ex)
            {

                string errormessage = ex.Message;
                MessageBox.Show(errormessage);
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidates frm = new FormCandidates())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));

        }
        public void clear()
        {
            mtEmployee.Text = "";
            mtName.Text = "";
            mtLastName.Text = "";
            mtNationalID.Text = "";
            mtSalary.Text = "";

        }

    }
}
