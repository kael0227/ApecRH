﻿namespace ApecRH
{
    partial class FormCandidates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.mtcanfilter = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtApprove = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.mtGoBack = new MetroFramework.Controls.MetroTile();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            this.candidateIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationalIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hireDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationalityIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inactiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.approvedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.candidatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            " ",
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(673, 107);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 30;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // mtcanfilter
            // 
            // 
            // 
            // 
            this.mtcanfilter.CustomButton.Image = null;
            this.mtcanfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtcanfilter.CustomButton.Name = "";
            this.mtcanfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcanfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcanfilter.CustomButton.TabIndex = 1;
            this.mtcanfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcanfilter.CustomButton.UseSelectable = true;
            this.mtcanfilter.CustomButton.Visible = false;
            this.mtcanfilter.Lines = new string[0];
            this.mtcanfilter.Location = new System.Drawing.Point(673, 60);
            this.mtcanfilter.MaxLength = 32767;
            this.mtcanfilter.Name = "mtcanfilter";
            this.mtcanfilter.PasswordChar = '\0';
            this.mtcanfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcanfilter.SelectedText = "";
            this.mtcanfilter.SelectionLength = 0;
            this.mtcanfilter.SelectionStart = 0;
            this.mtcanfilter.ShortcutsEnabled = true;
            this.mtcanfilter.Size = new System.Drawing.Size(121, 23);
            this.mtcanfilter.TabIndex = 29;
            this.mtcanfilter.UseSelectable = true;
            this.mtcanfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcanfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(599, 107);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 28;
            this.metroLabel2.Text = "Status";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(600, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(69, 19);
            this.metroLabel1.TabIndex = 27;
            this.metroLabel1.Text = "Candidate";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.candidateIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.nationalIDDataGridViewTextBoxColumn,
            this.hireDateDataGridViewTextBoxColumn,
            this.departmentIDDataGridViewTextBoxColumn,
            this.departmentDataGridViewTextBoxColumn,
            this.positionIDDataGridViewTextBoxColumn,
            this.positionDataGridViewTextBoxColumn,
            this.nationalityIDDataGridViewTextBoxColumn,
            this.Nationality,
            this.salaryDataGridViewTextBoxColumn,
            this.inactiveDataGridViewCheckBoxColumn,
            this.approvedDataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.candidatesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(10, 186);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1144, 281);
            this.dataGridView1.TabIndex = 26;
            // 
            // Nationality
            // 
            this.Nationality.DataPropertyName = "NationName";
            this.Nationality.HeaderText = "Nationality";
            this.Nationality.Name = "Nationality";
            this.Nationality.ReadOnly = true;
            // 
            // mtApprove
            // 
            this.mtApprove.ActiveControl = null;
            this.mtApprove.BackColor = System.Drawing.Color.Crimson;
            this.mtApprove.Location = new System.Drawing.Point(334, 98);
            this.mtApprove.Name = "mtApprove";
            this.mtApprove.Size = new System.Drawing.Size(75, 71);
            this.mtApprove.TabIndex = 25;
            this.mtApprove.Text = "Approve";
            this.mtApprove.UseCustomBackColor = true;
            this.mtApprove.UseSelectable = true;
            this.mtApprove.Click += new System.EventHandler(this.mtApprove_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(172, 99);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 24;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(253, 99);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 70);
            this.mtDelete.TabIndex = 23;
            this.mtDelete.Text = "Delete";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(91, 98);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 22;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(10, 98);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 21;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.mtSearch_Click);
            // 
            // mtGoBack
            // 
            this.mtGoBack.ActiveControl = null;
            this.mtGoBack.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtGoBack.Location = new System.Drawing.Point(496, 98);
            this.mtGoBack.Name = "mtGoBack";
            this.mtGoBack.Size = new System.Drawing.Size(76, 71);
            this.mtGoBack.TabIndex = 31;
            this.mtGoBack.Text = "Go Back";
            this.mtGoBack.UseCustomBackColor = true;
            this.mtGoBack.UseSelectable = true;
            this.mtGoBack.Click += new System.EventHandler(this.mtGoBack_Click);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.BackColor = System.Drawing.Color.BlueViolet;
            this.metroTile1.Location = new System.Drawing.Point(415, 98);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(75, 71);
            this.metroTile1.TabIndex = 32;
            this.metroTile1.Text = "Rejected";
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // candidateIDDataGridViewTextBoxColumn
            // 
            this.candidateIDDataGridViewTextBoxColumn.DataPropertyName = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.HeaderText = "CandidateID";
            this.candidateIDDataGridViewTextBoxColumn.Name = "candidateIDDataGridViewTextBoxColumn";
            this.candidateIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "LastName";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nationalIDDataGridViewTextBoxColumn
            // 
            this.nationalIDDataGridViewTextBoxColumn.DataPropertyName = "NationalID";
            this.nationalIDDataGridViewTextBoxColumn.HeaderText = "NationalID";
            this.nationalIDDataGridViewTextBoxColumn.Name = "nationalIDDataGridViewTextBoxColumn";
            this.nationalIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hireDateDataGridViewTextBoxColumn
            // 
            this.hireDateDataGridViewTextBoxColumn.DataPropertyName = "HireDate";
            this.hireDateDataGridViewTextBoxColumn.HeaderText = "ApplicationDate";
            this.hireDateDataGridViewTextBoxColumn.Name = "hireDateDataGridViewTextBoxColumn";
            this.hireDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departmentIDDataGridViewTextBoxColumn
            // 
            this.departmentIDDataGridViewTextBoxColumn.DataPropertyName = "DepartmentID";
            this.departmentIDDataGridViewTextBoxColumn.HeaderText = "DepartmentID";
            this.departmentIDDataGridViewTextBoxColumn.Name = "departmentIDDataGridViewTextBoxColumn";
            this.departmentIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.departmentIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // departmentDataGridViewTextBoxColumn
            // 
            this.departmentDataGridViewTextBoxColumn.DataPropertyName = "Department";
            this.departmentDataGridViewTextBoxColumn.HeaderText = "Department";
            this.departmentDataGridViewTextBoxColumn.Name = "departmentDataGridViewTextBoxColumn";
            this.departmentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // positionIDDataGridViewTextBoxColumn
            // 
            this.positionIDDataGridViewTextBoxColumn.DataPropertyName = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.HeaderText = "PositionID";
            this.positionIDDataGridViewTextBoxColumn.Name = "positionIDDataGridViewTextBoxColumn";
            this.positionIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.positionIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "Position";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nationalityIDDataGridViewTextBoxColumn
            // 
            this.nationalityIDDataGridViewTextBoxColumn.DataPropertyName = "NationalityID";
            this.nationalityIDDataGridViewTextBoxColumn.HeaderText = "NationalityID";
            this.nationalityIDDataGridViewTextBoxColumn.Name = "nationalityIDDataGridViewTextBoxColumn";
            this.nationalityIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.nationalityIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // salaryDataGridViewTextBoxColumn
            // 
            this.salaryDataGridViewTextBoxColumn.DataPropertyName = "Salary";
            this.salaryDataGridViewTextBoxColumn.HeaderText = "Salary";
            this.salaryDataGridViewTextBoxColumn.Name = "salaryDataGridViewTextBoxColumn";
            this.salaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inactiveDataGridViewCheckBoxColumn
            // 
            this.inactiveDataGridViewCheckBoxColumn.DataPropertyName = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.HeaderText = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.Name = "inactiveDataGridViewCheckBoxColumn";
            this.inactiveDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // approvedDataGridViewCheckBoxColumn
            // 
            this.approvedDataGridViewCheckBoxColumn.DataPropertyName = "Approved";
            this.approvedDataGridViewCheckBoxColumn.HeaderText = "Approved";
            this.approvedDataGridViewCheckBoxColumn.Name = "approvedDataGridViewCheckBoxColumn";
            this.approvedDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // candidatesBindingSource
            // 
            this.candidatesBindingSource.DataSource = typeof(ApecRH.Models.Candidates);
            // 
            // FormCandidates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1162, 594);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.mtGoBack);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.mtcanfilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtApprove);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormCandidates";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Candidates";
            this.Load += new System.EventHandler(this.FormCandidates_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private MetroFramework.Controls.MetroTextBox mtcanfilter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroTile mtApprove;
        public MetroFramework.Controls.MetroTile mtEdit;
        public MetroFramework.Controls.MetroTile mtDelete;
        public MetroFramework.Controls.MetroTile mtAddP;
        public MetroFramework.Controls.MetroTile mtSearch;
        private System.Windows.Forms.BindingSource candidatesBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationalityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn candidateIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationalIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hireDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationalityIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inactiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn approvedDataGridViewCheckBoxColumn;
        private MetroFramework.Controls.MetroTile mtGoBack;
        private MetroFramework.Controls.MetroTile metroTile1;
    }
}