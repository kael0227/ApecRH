﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class MenuAdministration : MetroFramework.Forms.MetroForm
    {
        public MenuAdministration()
        {
            InitializeComponent();
        }

        private void mtDepartments_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormDepartments form3 = new FormDepartments();
            form3.Show();
            this.Close();  
           


        }

        private void mtPosition_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(FormPositions frm = new FormPositions())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void mtgoback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu frm = new Menu();
            frm.ShowDialog();
            this.Dispose();
        }
    }
}
