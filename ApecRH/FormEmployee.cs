﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class FormEmployee : MetroFramework.Forms.MetroForm
    {
        public FormEmployee()
        {
            InitializeComponent();
        }

        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(FormCandidates frm = new FormCandidates())
            {
                frm.mtAddP.Visible = false;
                frm.mtDelete.Visible = false;
                frm.mtSearch.Visible = false;
                frm.mtEdit.Visible = false;
                frm.ShowDialog();
            }
            this.Dispose();
        }
        

        private void FormEmployee_Load(object sender, EventArgs e)
        {
            mcInactivefilter.SelectedItem = "Active";
            using (Models.Model1 db = new Models.Model1())
            {
                var result = from emp in db.Employees
                             join dep in db.Departments on emp.DepartmentRefID equals dep.DepartmentID
                             join pos in db.Positions on emp.PositionID equals pos.PositionID
                             join nat in db.Nationalities on emp.NationalitiesID equals nat.NationalitiesID
                             where emp.Inactive == false
                             select new
                             {
                                 emp.EmployeeID,
                                 emp.Name,
                                 emp.LastName,
                                 emp.NationalID,
                                 emp.HireDate,
                                 emp.DepartmentRefID,
                                 dep.Department,
                                 emp.PositionID,
                                 pos.Position,
                                 emp.NationalitiesID,
                                 nat.NationName,
                                 emp.Salary,
                                 emp.Inactive
                             };
                dataGridView1.DataSource = result.ToList(); 
            }
        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to fire this employee?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {
                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        var empD = db.Employees.First(c => c.EmployeeID == toBeDeleted);
                        empD.Inactive = true;
                        db.SaveChanges();
                        var result = from emp in db.Employees
                                     join dep in db.Departments on emp.DepartmentRefID equals dep.DepartmentID
                                     join pos in db.Positions on emp.PositionID equals pos.PositionID
                                     join nat in db.Nationalities on emp.NationalitiesID equals nat.NationalitiesID
                                     where emp.Inactive == false
                                     select new
                                     {
                                         emp.EmployeeID,
                                         emp.Name,
                                         emp.LastName,
                                         emp.NationalID,
                                         emp.HireDate,
                                         emp.DepartmentRefID,
                                         dep.Department,
                                         emp.PositionID,
                                         pos.Position,
                                         emp.NationalitiesID,
                                         nat.NationName,
                                         emp.Salary,
                                         emp.Inactive
                                     };
                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }

            catch (Exception)
            {

                throw;
            }
        }

        private void mtEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            CandidatesAddAndEdit frm = new CandidatesAddAndEdit();
            frm.empID = int.Parse(row.Cells[0].Value.ToString());
            frm.empName = row.Cells[1].Value.ToString();
            frm.empLastName = row.Cells[2].Value.ToString();
            frm.empNationalID = row.Cells[3].Value.ToString();
            frm.empHireDate = DateTime.Parse(row.Cells[4].Value.ToString());
            frm.empDep = int.Parse(row.Cells[5].Value.ToString());
            frm.empPos = int.Parse(row.Cells[7].Value.ToString());
            frm.empNation = row.Cells[9].Value.ToString();
            frm.empSalary = double.Parse(row.Cells[11].Value.ToString());
            frm.inactive = bool.Parse(row.Cells[12].Value.ToString());
            frm.edit = true;
            frm.empedit = true;
            frm.mtEmployee.Enabled = false;
            frm.btnsavequit.Hide();
            this.Hide();
            frm.ShowDialog();
            this.Dispose();
        }

        private void mtSearch_Click(object sender, EventArgs e)
        {
            Models.Model1 db = new Models.Model1();
            //query to modify for filters

            var result = from cand in db.Employees
                         join dep in db.Departments on cand.DepartmentRefID equals dep.DepartmentID
                         join pos in db.Positions on cand.PositionID equals pos.PositionID
                         join nat in db.Nationalities on cand.NationalitiesID equals nat.NationalitiesID
                         select new
                         {
                             cand.EmployeeID,
                             cand.Name,
                             cand.LastName,
                             cand.NationalID,
                             cand.HireDate,
                             cand.DepartmentRefID,
                             dep.Department,
                             cand.PositionID,
                             pos.Position,
                             cand.NationalitiesID,
                             nat.NationName,
                             cand.Salary,
                             cand.Inactive,
                       
                         };

            if (mtcanfilter.Text != "")
            {

               result = result.Where(c => c.EmployeeID.ToString().Contains(mtcanfilter.Text) ||
                                  c.Name.Contains(mtcanfilter.Text) ||
                                  c.LastName.Contains(mtcanfilter.Text));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtcanfilter.Text == "")
            {
                dataGridView1.DataSource = result.ToList();
            }

            if (mcInactivefilter.SelectedItem == "")
            {
                dataGridView1.DataSource = result.ToList();

            }
            else if (mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(c => c.Inactive == false);
                dataGridView1.DataSource = result.ToList();

            }
            else if (mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(c => c.Inactive == true);
                dataGridView1.DataSource = result.ToList();
            }


        }

        private void mtgoback_Click(object sender, EventArgs e)
        {
            this.Hide();
            Menu frm = new Menu();
            frm.ShowDialog();
            this.Dispose();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            printDocument1.Print();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(this.dataGridView1.Width, this.dataGridView1.Height);
            dataGridView1.DrawToBitmap(bm, new Rectangle(100, 200, this.dataGridView1.Width, this.dataGridView1.Height));
            e.Graphics.DrawImage(bm, 100, 200);
        }
    }
}
