﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class MenuRecruiment : MetroFramework.Forms.MetroForm
    {
        public MenuRecruiment()
        {
            InitializeComponent();
        }

        private void MtCandidates_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidates frm = new FormCandidates())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtcwe_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormCandidatesWorkExperience frm = new FormCandidatesWorkExperience();
            frm.ShowDialog();
            this.Dispose();
        }

        private void mtEduc_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidatesEducation frm = new FormCandidatesEducation())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void Mtskills_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(FormCandidatesSkills frm = new FormCandidatesSkills())
            {
                frm.ShowDialog();
            }
            this.Hide();
        }

        private void mtsalir_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(Menu frm = new Menu())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void mtlan_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(FormCandidatesLanguages frm = new FormCandidatesLanguages())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }
    }
}
