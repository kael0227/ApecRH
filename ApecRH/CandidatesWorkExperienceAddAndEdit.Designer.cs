﻿namespace ApecRH
{
    partial class CandidatesWorkExperienceAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MtDateEnd = new MetroFramework.Controls.MetroDateTime();
            this.MtDateBeg = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.MtPosition = new MetroFramework.Controls.MetroComboBox();
            this.candidatesWorkExperienceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MtSalary = new MetroFramework.Controls.MetroTextBox();
            this.mtCandidate = new MetroFramework.Controls.MetroTextBox();
            this.mtOrganization = new MetroFramework.Controls.MetroTextBox();
            this.mtcandwork = new MetroFramework.Controls.MetroTextBox();
            this.btnsavequit = new MetroFramework.Controls.MetroButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesWorkExperienceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MtDateEnd);
            this.groupBox1.Controls.Add(this.MtDateBeg);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.MtPosition);
            this.groupBox1.Controls.Add(this.MtSalary);
            this.groupBox1.Controls.Add(this.mtCandidate);
            this.groupBox1.Controls.Add(this.mtOrganization);
            this.groupBox1.Controls.Add(this.mtcandwork);
            this.groupBox1.Location = new System.Drawing.Point(86, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 478);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "WorkExp";
            // 
            // MtDateEnd
            // 
            this.MtDateEnd.Location = new System.Drawing.Point(200, 284);
            this.MtDateEnd.MinimumSize = new System.Drawing.Size(0, 29);
            this.MtDateEnd.Name = "MtDateEnd";
            this.MtDateEnd.Size = new System.Drawing.Size(178, 29);
            this.MtDateEnd.TabIndex = 33;
            // 
            // MtDateBeg
            // 
            this.MtDateBeg.Location = new System.Drawing.Point(199, 237);
            this.MtDateBeg.MinimumSize = new System.Drawing.Size(0, 29);
            this.MtDateBeg.Name = "MtDateBeg";
            this.MtDateBeg.Size = new System.Drawing.Size(178, 29);
            this.MtDateBeg.TabIndex = 32;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(80, 338);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(45, 19);
            this.metroLabel7.TabIndex = 31;
            this.metroLabel7.Text = "Salary";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(80, 284);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(58, 19);
            this.metroLabel6.TabIndex = 30;
            this.metroLabel6.Text = "DateEnd";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(80, 191);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(54, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Position";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(80, 237);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(59, 19);
            this.metroLabel4.TabIndex = 28;
            this.metroLabel4.Text = "DateBeg";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(80, 151);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(85, 19);
            this.metroLabel3.TabIndex = 27;
            this.metroLabel3.Text = "Organization";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(80, 106);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(81, 19);
            this.metroLabel2.TabIndex = 26;
            this.metroLabel2.Text = "CandidateID";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(80, 56);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(73, 19);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "WorkExpID";
            this.metroLabel1.Visible = false;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(152, 419);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(286, 419);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // MtPosition
            // 
            this.MtPosition.DataSource = this.candidatesWorkExperienceBindingSource;
            this.MtPosition.DisplayMember = "Position";
            this.MtPosition.FormattingEnabled = true;
            this.MtPosition.ItemHeight = 23;
            this.MtPosition.Location = new System.Drawing.Point(200, 191);
            this.MtPosition.Name = "MtPosition";
            this.MtPosition.Size = new System.Drawing.Size(177, 29);
            this.MtPosition.TabIndex = 21;
            this.MtPosition.UseSelectable = true;
            this.MtPosition.ValueMember = "PositionID";
            // 
            // candidatesWorkExperienceBindingSource
            // 
            this.candidatesWorkExperienceBindingSource.DataSource = typeof(ApecRH.Models.CandidatesWorkExperience);
            // 
            // MtSalary
            // 
            // 
            // 
            // 
            this.MtSalary.CustomButton.Image = null;
            this.MtSalary.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.MtSalary.CustomButton.Name = "";
            this.MtSalary.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.MtSalary.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.MtSalary.CustomButton.TabIndex = 1;
            this.MtSalary.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MtSalary.CustomButton.UseSelectable = true;
            this.MtSalary.CustomButton.Visible = false;
            this.MtSalary.Lines = new string[0];
            this.MtSalary.Location = new System.Drawing.Point(200, 334);
            this.MtSalary.MaxLength = 32767;
            this.MtSalary.Name = "MtSalary";
            this.MtSalary.PasswordChar = '\0';
            this.MtSalary.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.MtSalary.SelectedText = "";
            this.MtSalary.SelectionLength = 0;
            this.MtSalary.SelectionStart = 0;
            this.MtSalary.ShortcutsEnabled = true;
            this.MtSalary.Size = new System.Drawing.Size(177, 23);
            this.MtSalary.TabIndex = 20;
            this.MtSalary.UseSelectable = true;
            this.MtSalary.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.MtSalary.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.MtSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtCandidate
            // 
            // 
            // 
            // 
            this.mtCandidate.CustomButton.Image = null;
            this.mtCandidate.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtCandidate.CustomButton.Name = "";
            this.mtCandidate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtCandidate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtCandidate.CustomButton.TabIndex = 1;
            this.mtCandidate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtCandidate.CustomButton.UseSelectable = true;
            this.mtCandidate.CustomButton.Visible = false;
            this.mtCandidate.Lines = new string[0];
            this.mtCandidate.Location = new System.Drawing.Point(200, 106);
            this.mtCandidate.MaxLength = 32767;
            this.mtCandidate.Name = "mtCandidate";
            this.mtCandidate.PasswordChar = '\0';
            this.mtCandidate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtCandidate.SelectedText = "";
            this.mtCandidate.SelectionLength = 0;
            this.mtCandidate.SelectionStart = 0;
            this.mtCandidate.ShortcutsEnabled = true;
            this.mtCandidate.Size = new System.Drawing.Size(177, 23);
            this.mtCandidate.TabIndex = 19;
            this.mtCandidate.UseSelectable = true;
            this.mtCandidate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtCandidate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtCandidate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtOrganization
            // 
            // 
            // 
            // 
            this.mtOrganization.CustomButton.Image = null;
            this.mtOrganization.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtOrganization.CustomButton.Name = "";
            this.mtOrganization.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtOrganization.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtOrganization.CustomButton.TabIndex = 1;
            this.mtOrganization.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtOrganization.CustomButton.UseSelectable = true;
            this.mtOrganization.CustomButton.Visible = false;
            this.mtOrganization.Lines = new string[0];
            this.mtOrganization.Location = new System.Drawing.Point(200, 151);
            this.mtOrganization.MaxLength = 32767;
            this.mtOrganization.Name = "mtOrganization";
            this.mtOrganization.PasswordChar = '\0';
            this.mtOrganization.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtOrganization.SelectedText = "";
            this.mtOrganization.SelectionLength = 0;
            this.mtOrganization.SelectionStart = 0;
            this.mtOrganization.ShortcutsEnabled = true;
            this.mtOrganization.Size = new System.Drawing.Size(177, 23);
            this.mtOrganization.TabIndex = 18;
            this.mtOrganization.UseSelectable = true;
            this.mtOrganization.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtOrganization.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtOrganization.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // mtcandwork
            // 
            // 
            // 
            // 
            this.mtcandwork.CustomButton.Image = null;
            this.mtcandwork.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtcandwork.CustomButton.Name = "";
            this.mtcandwork.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcandwork.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcandwork.CustomButton.TabIndex = 1;
            this.mtcandwork.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcandwork.CustomButton.UseSelectable = true;
            this.mtcandwork.CustomButton.Visible = false;
            this.mtcandwork.Lines = new string[0];
            this.mtcandwork.Location = new System.Drawing.Point(200, 56);
            this.mtcandwork.MaxLength = 32767;
            this.mtcandwork.Name = "mtcandwork";
            this.mtcandwork.PasswordChar = '\0';
            this.mtcandwork.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcandwork.SelectedText = "";
            this.mtcandwork.SelectionLength = 0;
            this.mtcandwork.SelectionStart = 0;
            this.mtcandwork.ShortcutsEnabled = true;
            this.mtcandwork.Size = new System.Drawing.Size(177, 23);
            this.mtcandwork.TabIndex = 17;
            this.mtcandwork.UseSelectable = true;
            this.mtcandwork.Visible = false;
            this.mtcandwork.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcandwork.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtcandwork.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // btnsavequit
            // 
            this.btnsavequit.Location = new System.Drawing.Point(574, 422);
            this.btnsavequit.Name = "btnsavequit";
            this.btnsavequit.Size = new System.Drawing.Size(100, 23);
            this.btnsavequit.TabIndex = 24;
            this.btnsavequit.Text = "Save And Quit";
            this.btnsavequit.UseSelectable = true;
            this.btnsavequit.Visible = false;
            // 
            // CandidatesWorkExperienceAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 577);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnsavequit);
            this.Name = "CandidatesWorkExperienceAddAndEdit";
            this.Text = "CandidatesWorkExperienceAddAndEdit";
            this.Load += new System.EventHandler(this.CandidatesWorkExperienceAddAndEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesWorkExperienceBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroButton btnsavequit;
        private MetroFramework.Controls.MetroButton BtnCancel;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroComboBox MtPosition;
        private MetroFramework.Controls.MetroTextBox MtSalary;
        private MetroFramework.Controls.MetroTextBox mtCandidate;
        private MetroFramework.Controls.MetroTextBox mtOrganization;
        public MetroFramework.Controls.MetroTextBox mtcandwork;
        private MetroFramework.Controls.MetroDateTime MtDateEnd;
        private MetroFramework.Controls.MetroDateTime MtDateBeg;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.BindingSource candidatesWorkExperienceBindingSource;
    }
}