﻿namespace ApecRH
{
    partial class MenuRecruiment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtsalir = new MetroFramework.Controls.MetroTile();
            this.mtlan = new MetroFramework.Controls.MetroTile();
            this.Mtskills = new MetroFramework.Controls.MetroTile();
            this.mtEduc = new MetroFramework.Controls.MetroTile();
            this.mtcwe = new MetroFramework.Controls.MetroTile();
            this.MtCandidates = new MetroFramework.Controls.MetroTile();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtsalir);
            this.groupBox1.Controls.Add(this.mtlan);
            this.groupBox1.Controls.Add(this.Mtskills);
            this.groupBox1.Controls.Add(this.mtEduc);
            this.groupBox1.Controls.Add(this.mtcwe);
            this.groupBox1.Controls.Add(this.MtCandidates);
            this.groupBox1.Location = new System.Drawing.Point(44, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(453, 359);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Options";
            // 
            // mtsalir
            // 
            this.mtsalir.ActiveControl = null;
            this.mtsalir.BackColor = System.Drawing.Color.CornflowerBlue;
            this.mtsalir.Location = new System.Drawing.Point(287, 260);
            this.mtsalir.Name = "mtsalir";
            this.mtsalir.Size = new System.Drawing.Size(113, 82);
            this.mtsalir.TabIndex = 5;
            this.mtsalir.Text = "Go back";
            this.mtsalir.UseCustomBackColor = true;
            this.mtsalir.UseSelectable = true;
            this.mtsalir.Click += new System.EventHandler(this.mtsalir_Click);
            // 
            // mtlan
            // 
            this.mtlan.ActiveControl = null;
            this.mtlan.BackColor = System.Drawing.Color.SeaGreen;
            this.mtlan.Location = new System.Drawing.Point(25, 260);
            this.mtlan.Name = "mtlan";
            this.mtlan.Size = new System.Drawing.Size(113, 82);
            this.mtlan.TabIndex = 4;
            this.mtlan.Text = "Languague";
            this.mtlan.UseCustomBackColor = true;
            this.mtlan.UseSelectable = true;
            this.mtlan.Click += new System.EventHandler(this.mtlan_Click);
            // 
            // Mtskills
            // 
            this.Mtskills.ActiveControl = null;
            this.Mtskills.BackColor = System.Drawing.Color.DarkOrange;
            this.Mtskills.Location = new System.Drawing.Point(287, 141);
            this.Mtskills.Name = "Mtskills";
            this.Mtskills.Size = new System.Drawing.Size(113, 82);
            this.Mtskills.TabIndex = 3;
            this.Mtskills.Text = "Skills";
            this.Mtskills.UseCustomBackColor = true;
            this.Mtskills.UseSelectable = true;
            this.Mtskills.Click += new System.EventHandler(this.Mtskills_Click);
            // 
            // mtEduc
            // 
            this.mtEduc.ActiveControl = null;
            this.mtEduc.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.mtEduc.Location = new System.Drawing.Point(25, 141);
            this.mtEduc.Name = "mtEduc";
            this.mtEduc.Size = new System.Drawing.Size(113, 82);
            this.mtEduc.TabIndex = 2;
            this.mtEduc.Text = "Education";
            this.mtEduc.UseCustomBackColor = true;
            this.mtEduc.UseSelectable = true;
            this.mtEduc.Click += new System.EventHandler(this.mtEduc_Click);
            // 
            // mtcwe
            // 
            this.mtcwe.ActiveControl = null;
            this.mtcwe.BackColor = System.Drawing.Color.DarkRed;
            this.mtcwe.Location = new System.Drawing.Point(287, 29);
            this.mtcwe.Name = "mtcwe";
            this.mtcwe.Size = new System.Drawing.Size(113, 82);
            this.mtcwe.TabIndex = 1;
            this.mtcwe.Text = "WorkExperience";
            this.mtcwe.UseCustomBackColor = true;
            this.mtcwe.UseSelectable = true;
            this.mtcwe.Click += new System.EventHandler(this.mtcwe_Click);
            // 
            // MtCandidates
            // 
            this.MtCandidates.ActiveControl = null;
            this.MtCandidates.BackColor = System.Drawing.Color.Crimson;
            this.MtCandidates.Location = new System.Drawing.Point(25, 29);
            this.MtCandidates.Name = "MtCandidates";
            this.MtCandidates.Size = new System.Drawing.Size(113, 82);
            this.MtCandidates.TabIndex = 0;
            this.MtCandidates.Text = "Candidates";
            this.MtCandidates.UseCustomBackColor = true;
            this.MtCandidates.UseSelectable = true;
            this.MtCandidates.Click += new System.EventHandler(this.MtCandidates_Click);
            // 
            // MenuRecruiment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 486);
            this.Controls.Add(this.groupBox1);
            this.Name = "MenuRecruiment";
            this.Style = MetroFramework.MetroColorStyle.Brown;
            this.Text = "MenuRecruiment";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTile mtcwe;
        private MetroFramework.Controls.MetroTile MtCandidates;
        private MetroFramework.Controls.MetroTile Mtskills;
        private MetroFramework.Controls.MetroTile mtEduc;
        private MetroFramework.Controls.MetroTile mtsalir;
        private MetroFramework.Controls.MetroTile mtlan;
    }
}