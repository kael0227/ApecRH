﻿namespace ApecRH
{
    partial class FormEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mcInactivefilter = new MetroFramework.Controls.MetroComboBox();
            this.mtcanfilter = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.DepartmentRefID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PositionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NationalitiesID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtgoback = new MetroFramework.Controls.MetroTile();
            this.mtEdit = new MetroFramework.Controls.MetroTile();
            this.mtDelete = new MetroFramework.Controls.MetroTile();
            this.mtAddP = new MetroFramework.Controls.MetroTile();
            this.mtSearch = new MetroFramework.Controls.MetroTile();
            this.employeeIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationalIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hireDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departmentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inactiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.employeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.metroTile1 = new MetroFramework.Controls.MetroTile();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mcInactivefilter
            // 
            this.mcInactivefilter.FormattingEnabled = true;
            this.mcInactivefilter.ItemHeight = 23;
            this.mcInactivefilter.Items.AddRange(new object[] {
            " ",
            "Active",
            "Inactive"});
            this.mcInactivefilter.Location = new System.Drawing.Point(512, 114);
            this.mcInactivefilter.Name = "mcInactivefilter";
            this.mcInactivefilter.Size = new System.Drawing.Size(121, 29);
            this.mcInactivefilter.TabIndex = 20;
            this.mcInactivefilter.UseSelectable = true;
            // 
            // mtcanfilter
            // 
            // 
            // 
            // 
            this.mtcanfilter.CustomButton.Image = null;
            this.mtcanfilter.CustomButton.Location = new System.Drawing.Point(99, 1);
            this.mtcanfilter.CustomButton.Name = "";
            this.mtcanfilter.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcanfilter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcanfilter.CustomButton.TabIndex = 1;
            this.mtcanfilter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcanfilter.CustomButton.UseSelectable = true;
            this.mtcanfilter.CustomButton.Visible = false;
            this.mtcanfilter.Lines = new string[0];
            this.mtcanfilter.Location = new System.Drawing.Point(512, 67);
            this.mtcanfilter.MaxLength = 32767;
            this.mtcanfilter.Name = "mtcanfilter";
            this.mtcanfilter.PasswordChar = '\0';
            this.mtcanfilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcanfilter.SelectedText = "";
            this.mtcanfilter.SelectionLength = 0;
            this.mtcanfilter.SelectionStart = 0;
            this.mtcanfilter.ShortcutsEnabled = true;
            this.mtcanfilter.Size = new System.Drawing.Size(121, 23);
            this.mtcanfilter.TabIndex = 19;
            this.mtcanfilter.UseSelectable = true;
            this.mtcanfilter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcanfilter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(438, 114);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 18;
            this.metroLabel2.Text = "Status";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(439, 67);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(67, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Employee";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.employeeIDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.nationalIDDataGridViewTextBoxColumn,
            this.hireDateDataGridViewTextBoxColumn,
            this.DepartmentRefID,
            this.departmentDataGridViewTextBoxColumn,
            this.PositionID,
            this.positionDataGridViewTextBoxColumn,
            this.NationalitiesID,
            this.Nationality,
            this.salaryDataGridViewTextBoxColumn,
            this.inactiveDataGridViewCheckBoxColumn});
            this.dataGridView1.DataSource = this.employeesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(24, 175);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1046, 281);
            this.dataGridView1.TabIndex = 16;
            // 
            // DepartmentRefID
            // 
            this.DepartmentRefID.DataPropertyName = "DepartmentRefID";
            this.DepartmentRefID.HeaderText = "DepartmentRefID";
            this.DepartmentRefID.Name = "DepartmentRefID";
            this.DepartmentRefID.ReadOnly = true;
            this.DepartmentRefID.Visible = false;
            // 
            // PositionID
            // 
            this.PositionID.DataPropertyName = "PositionID";
            this.PositionID.HeaderText = "PositionID";
            this.PositionID.Name = "PositionID";
            this.PositionID.ReadOnly = true;
            this.PositionID.Visible = false;
            // 
            // NationalitiesID
            // 
            this.NationalitiesID.DataPropertyName = "NationalitiesID";
            this.NationalitiesID.HeaderText = "NationalitiesID";
            this.NationalitiesID.Name = "NationalitiesID";
            this.NationalitiesID.ReadOnly = true;
            this.NationalitiesID.Visible = false;
            // 
            // Nationality
            // 
            this.Nationality.DataPropertyName = "NationName";
            this.Nationality.HeaderText = "Nationality";
            this.Nationality.Name = "Nationality";
            this.Nationality.ReadOnly = true;
            // 
            // mtgoback
            // 
            this.mtgoback.ActiveControl = null;
            this.mtgoback.BackColor = System.Drawing.Color.RoyalBlue;
            this.mtgoback.Location = new System.Drawing.Point(347, 98);
            this.mtgoback.Name = "mtgoback";
            this.mtgoback.Size = new System.Drawing.Size(75, 70);
            this.mtgoback.TabIndex = 15;
            this.mtgoback.Text = "Go Back";
            this.mtgoback.UseCustomBackColor = true;
            this.mtgoback.UseSelectable = true;
            this.mtgoback.Click += new System.EventHandler(this.mtgoback_Click);
            // 
            // mtEdit
            // 
            this.mtEdit.ActiveControl = null;
            this.mtEdit.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.mtEdit.Location = new System.Drawing.Point(185, 99);
            this.mtEdit.Name = "mtEdit";
            this.mtEdit.Size = new System.Drawing.Size(75, 70);
            this.mtEdit.TabIndex = 14;
            this.mtEdit.Text = "Edit";
            this.mtEdit.UseCustomBackColor = true;
            this.mtEdit.UseSelectable = true;
            this.mtEdit.Click += new System.EventHandler(this.mtEdit_Click);
            // 
            // mtDelete
            // 
            this.mtDelete.ActiveControl = null;
            this.mtDelete.BackColor = System.Drawing.Color.DarkRed;
            this.mtDelete.Location = new System.Drawing.Point(266, 99);
            this.mtDelete.Name = "mtDelete";
            this.mtDelete.Size = new System.Drawing.Size(75, 70);
            this.mtDelete.TabIndex = 13;
            this.mtDelete.Text = "Fire";
            this.mtDelete.UseCustomBackColor = true;
            this.mtDelete.UseSelectable = true;
            this.mtDelete.Click += new System.EventHandler(this.mtDelete_Click);
            // 
            // mtAddP
            // 
            this.mtAddP.ActiveControl = null;
            this.mtAddP.BackColor = System.Drawing.Color.DarkGreen;
            this.mtAddP.Location = new System.Drawing.Point(104, 98);
            this.mtAddP.Name = "mtAddP";
            this.mtAddP.Size = new System.Drawing.Size(75, 70);
            this.mtAddP.TabIndex = 12;
            this.mtAddP.Text = "Add";
            this.mtAddP.UseCustomBackColor = true;
            this.mtAddP.UseSelectable = true;
            this.mtAddP.Click += new System.EventHandler(this.mtAddP_Click);
            // 
            // mtSearch
            // 
            this.mtSearch.ActiveControl = null;
            this.mtSearch.BackColor = System.Drawing.Color.Navy;
            this.mtSearch.Location = new System.Drawing.Point(23, 98);
            this.mtSearch.Name = "mtSearch";
            this.mtSearch.Size = new System.Drawing.Size(75, 70);
            this.mtSearch.TabIndex = 11;
            this.mtSearch.Text = "Search";
            this.mtSearch.UseCustomBackColor = true;
            this.mtSearch.UseSelectable = true;
            this.mtSearch.Click += new System.EventHandler(this.mtSearch_Click);
            // 
            // employeeIDDataGridViewTextBoxColumn
            // 
            this.employeeIDDataGridViewTextBoxColumn.DataPropertyName = "EmployeeID";
            this.employeeIDDataGridViewTextBoxColumn.HeaderText = "EmployeeID";
            this.employeeIDDataGridViewTextBoxColumn.Name = "employeeIDDataGridViewTextBoxColumn";
            this.employeeIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "LastName";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nationalIDDataGridViewTextBoxColumn
            // 
            this.nationalIDDataGridViewTextBoxColumn.DataPropertyName = "NationalID";
            this.nationalIDDataGridViewTextBoxColumn.HeaderText = "NationalID";
            this.nationalIDDataGridViewTextBoxColumn.Name = "nationalIDDataGridViewTextBoxColumn";
            this.nationalIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // hireDateDataGridViewTextBoxColumn
            // 
            this.hireDateDataGridViewTextBoxColumn.DataPropertyName = "HireDate";
            this.hireDateDataGridViewTextBoxColumn.HeaderText = "HireDate";
            this.hireDateDataGridViewTextBoxColumn.Name = "hireDateDataGridViewTextBoxColumn";
            this.hireDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // departmentDataGridViewTextBoxColumn
            // 
            this.departmentDataGridViewTextBoxColumn.DataPropertyName = "Department";
            this.departmentDataGridViewTextBoxColumn.HeaderText = "Department";
            this.departmentDataGridViewTextBoxColumn.Name = "departmentDataGridViewTextBoxColumn";
            this.departmentDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "Position";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // salaryDataGridViewTextBoxColumn
            // 
            this.salaryDataGridViewTextBoxColumn.DataPropertyName = "Salary";
            this.salaryDataGridViewTextBoxColumn.HeaderText = "Salary";
            this.salaryDataGridViewTextBoxColumn.Name = "salaryDataGridViewTextBoxColumn";
            this.salaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // inactiveDataGridViewCheckBoxColumn
            // 
            this.inactiveDataGridViewCheckBoxColumn.DataPropertyName = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.HeaderText = "Inactive";
            this.inactiveDataGridViewCheckBoxColumn.Name = "inactiveDataGridViewCheckBoxColumn";
            this.inactiveDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // employeesBindingSource
            // 
            this.employeesBindingSource.DataSource = typeof(ApecRH.Models.Employees);
            // 
            // metroTile1
            // 
            this.metroTile1.ActiveControl = null;
            this.metroTile1.BackColor = System.Drawing.Color.Silver;
            this.metroTile1.Location = new System.Drawing.Point(683, 63);
            this.metroTile1.Name = "metroTile1";
            this.metroTile1.Size = new System.Drawing.Size(93, 69);
            this.metroTile1.TabIndex = 21;
            this.metroTile1.Text = "Print";
            this.metroTile1.UseCustomBackColor = true;
            this.metroTile1.UseSelectable = true;
            this.metroTile1.Click += new System.EventHandler(this.metroTile1_Click);
            // 
            // FormEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1076, 523);
            this.Controls.Add(this.metroTile1);
            this.Controls.Add(this.mcInactivefilter);
            this.Controls.Add(this.mtcanfilter);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.mtgoback);
            this.Controls.Add(this.mtEdit);
            this.Controls.Add(this.mtDelete);
            this.Controls.Add(this.mtAddP);
            this.Controls.Add(this.mtSearch);
            this.Name = "FormEmployee";
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.Text = "Employee";
            this.Load += new System.EventHandler(this.FormEmployee_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employeesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox mcInactivefilter;
        private MetroFramework.Controls.MetroTextBox mtcanfilter;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource employeesBindingSource;
        private MetroFramework.Controls.MetroTile mtgoback;
        private MetroFramework.Controls.MetroTile mtEdit;
        private MetroFramework.Controls.MetroTile mtDelete;
        private MetroFramework.Controls.MetroTile mtAddP;
        private MetroFramework.Controls.MetroTile mtSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeeIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationalIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hireDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DepartmentRefID;
        private System.Windows.Forms.DataGridViewTextBoxColumn departmentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PositionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NationalitiesID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn salaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn inactiveDataGridViewCheckBoxColumn;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private MetroFramework.Controls.MetroTile metroTile1;
    }
}