﻿using ApecRH.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ApecRH
{
    public partial class DepartmentsAddAndEdit : MetroFramework.Forms.MetroForm
    {
        Departments Departments = new Departments();
        public int DepID;
        public string DepName;
        public bool Inactive;
        public bool edit = false;



        public DepartmentsAddAndEdit()
        {
            InitializeComponent();

        }






        private void DepartmentsAddAndEdit_Load(object sender, EventArgs e)
        {
            if (edit == true)
            {
                mtxtDepID.Text = DepID.ToString();
                mtDep.Text = DepName;
                MtStatus.Checked = Inactive;

            }
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }


        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (edit == false)
            {
                Model1 db = new Model1();

                DepID = int.Parse(mtxtDepID.Text);
                DepName = mtDep.Text;
                Inactive = MtStatus.Checked;

                Departments.DepartmentID = DepID;
                if (db.Departments.Any(d => d.DepartmentID == DepID))
                {
                    MessageBox.Show("Code Exists. Please enter a different code.");
                    mtxtDepID.Focus();
                    return;
                }
                Departments.Department = DepName;
                if (Inactive == true)
                {
                    Departments.Inactive = true;
                }
                else
                {
                    Departments.Inactive = false;
                }
                if (mtDep.Text == "" || mtxtDepID.Text == "")
                {
                    MessageBox.Show("Please fill all the textbox");
                    return;
                }

                try
                {
                    db.Departments.Add(Departments);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    Clear();

                }
                catch (Exception ex)
                {

                    string errorsmg = ex.Message;
                }

            }

            else
            {
                try
                {
                    using (Model1 db = new Model1())
                    {
                        var DepU = db.Departments.Where(x => x.DepartmentID == DepID).Select(x => x).FirstOrDefault();
                        DepName = mtDep.Text;
                        Inactive = MtStatus.Checked;

                        DepU.Department = DepName;
                        DepU.Inactive = Inactive;
                        if (mtDep.Text == "" || mtxtDepID.Text == "")
                        {
                            MessageBox.Show("Please fill all the textbox");
                            return;
                        }
                        if (db.Candidates.Any(p => p.DepartmentID == DepID) && Inactive == true)
                        {
                            MessageBox.Show("Can't Inactive record. It has dependency");
                            return;
                        }
                        db.SaveChanges();
                        db.Dispose();
                        this.Hide();
                        FormDepartments frm2 = new FormDepartments();
                        frm2.ShowDialog();
                        this.Dispose();

                    }
                }
                catch (Exception ex)
                {

                    string errormsg = ex.Message;
                }

            }
        }
        public void Clear()
        {
            mtDep.Text = "";
            mtxtDepID.Text = "";
            if (MtStatus.Checked == true)
            {
                MtStatus.Checked = false;
            }

        }

        private void btnSaveAndQuit_Click(object sender, EventArgs e)
        {
            DepID = int.Parse(mtxtDepID.Text);
            DepName = mtDep.Text;
            Inactive = MtStatus.Checked;

            Departments.DepartmentID = DepID;
            Departments.Department = DepName;
            if (Inactive == true)
            {
                Departments.Inactive = true;
            }
            else
            {
                Departments.Inactive = false;
            }
            using (Model1 db = new Model1())
            {
                try
                {
                    db.Departments.Add(Departments);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Se ha guardado correctamente");
                    Clear();
                    this.Dispose();
                    using (FormDepartments frm = new FormDepartments())
                    {
                        frm.ShowDialog();
                    }

                }
                catch (Exception)
                {

                    MessageBox.Show("Registro o Clave Duplicada. Por favor instertar diferente ID.");
                }

            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormDepartments frm = new FormDepartments())
            {
                frm.ShowDialog();
            }
        }

        private void mtxtDepID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }



        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }
    }
}
