﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class CandidatesSkillsAddAndEdit : MetroFramework.Forms.MetroForm
    {
        public int Cande;
        public int CandidateID;
        public string Skill;
        public string Level;
        public bool edit = false;
        public CandidatesSkillsAddAndEdit()
        {
            InitializeComponent();
        }

        private void CandidatesSkillsAddAndEdit_Load(object sender, EventArgs e)
        {
            McSkill.DataSource = Enum.GetNames(typeof(Skills));
            

            if (edit == true)
            {
                mtcandwork.Text = Cande.ToString();
                mtCandidate.Text = CandidateID.ToString();
                McSkill.SelectedItem = Skill;
                MtLevel.SelectedItem = Level;
            }

            using (Model1 db = new Model1())
            {
                if (!db.Candidates.Any(c => c.Inactive == false))
                {
                    MessageBox.Show("There's no any Candidate please insert one.");
                    FormCandidates frm = new FormCandidates();
                    this.Hide();
                    frm.ShowDialog();
                    this.Dispose();
                }
                var query = from c in db.Candidates
                            where c.Inactive == false
                            select c.CandidateID.ToString();



                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                source.AddRange(query.ToArray());
                this.mtCandidate.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.mtCandidate.AutoCompleteSource = AutoCompleteSource.CustomSource;
                this.mtCandidate.AutoCompleteCustomSource = source;

            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (edit == false)
            {
                Model1 db = new Model1();
                try
                {

                    CandidateID = int.Parse(mtCandidate.Text);
                    Skill = McSkill.SelectedItem.ToString();
                    Level = MtLevel.SelectedItem.ToString();

                    CandidatesSkills cwe = new CandidatesSkills();


                    cwe.CandidateID = CandidateID;
                    cwe.Skill = Skill;
                    cwe.Level = Level;

                    if(mtCandidate.Text == "")
                    {
                        MessageBox.Show("Please fill all the textbox");
                        return;
                    }

                    if (db.CandidatesSkills.Any(ck => ck.CandidateID == CandidateID && ck.Skill == Skill))
                    {
                        MessageBox.Show("Skill already exists. Select a new one.");
                        return;
                    }





                    db.CandidatesSkills.Add(cwe);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    clear();



                }
                catch (Exception ex)
                {

                    string errormessage = ex.Message;
                }



            }
            else
            {


                using (Model1 db = new Model1())
                {
                    var CanweU = db.CandidatesSkills.Where(x => x.CandidateEducation == Cande).Select(x => x).FirstOrDefault();
                    CandidateID = int.Parse(mtCandidate.Text);
                    Skill = McSkill.SelectedItem.ToString();
                    Level = MtLevel.SelectedItem.ToString();



                    if (mtCandidate.Text == "")
                    {
                        MessageBox.Show("Please fill all the textbox");
                        return;
                    }

                    CanweU.CandidateID = CandidateID;
                    CanweU.Skill = Skill;
                    CanweU.Level = Level;

                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    this.Hide();
                    using (FormCandidatesSkills frm = new FormCandidatesSkills())
                    {
                        frm.ShowDialog();
                    }
                    this.Dispose();

                }


            }

        }
        public void clear()
        {
            mtCandidate.Text = "";
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormCandidatesSkills frm = new FormCandidatesSkills())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }
        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));

        }
    }
}
