﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class FormCandidatesEducation : MetroFramework.Forms.MetroForm
    {
        public FormCandidatesEducation()
        {
            InitializeComponent();
        }

        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            using(CandidatesEducationAddAndEdit frm = new CandidatesEducationAddAndEdit())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void FormCandidatesEducation_Load(object sender, EventArgs e)
        {
            using (Model1 db = new Model1())
            {
                mcInactivefilter.SelectedItem = "Active";
                var result = from ce in db.CandidatesEducation
                             join c in db.Candidates on ce.CandidateID equals c.CandidateID
                             where c.Inactive == false
                             select new
                             {
                                 ce.CandidateEducation,
                                 ce.CandidateID,
                                 Name = c.Name + " " + c.LastName,
                                 ce.Institute,
                                 ce.Level,
                                 ce.DateBeg,
                                 ce.DateEnd,
                                 ce.Career
                             };
                dataGridView1.DataSource = result.ToList();

            }
        }

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to delete this record?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {
                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        var CanD = db.CandidatesEducation.First(c => c.CandidateEducation == toBeDeleted);
                        var notToBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[1].Value;
                        bool inactiveD = (from d in db.Candidates
                                          where d.CandidateID == notToBeDeleted
                                          select d.Inactive).Single();
                        if (inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar datos Inactivos. Contacte al supervisor");
                            return;
                        }
                        db.CandidatesEducation.Remove(CanD);
                        db.SaveChanges();
                        var result = from ce in db.CandidatesEducation
                                     join c in db.Candidates on ce.CandidateID equals c.CandidateID
                                     select new
                                     {
                                         ce.CandidateEducation,
                                         ce.CandidateID,
                                         Name = c.Name + " " + c.LastName,
                                         ce.Institute,
                                         ce.Level,
                                         ce.DateBeg,
                                         ce.DateEnd,
                                         ce.Career
                                     };

                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void mtEdit_Click(object sender, EventArgs e)
        {
            using(CandidatesEducationAddAndEdit frm = new CandidatesEducationAddAndEdit())
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Please Select a row");
                    return;
                }
                DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                frm.CandeID = int.Parse(row.Cells[0].Value.ToString());
                frm.CandidateID = int.Parse(row.Cells[1].Value.ToString());
                frm.Institute = row.Cells[3].Value.ToString();
                frm.Career = row.Cells[4].Value.ToString();
                frm.Level = row.Cells[5].Value.ToString();
                frm.DateBeg = DateTime.Parse(row.Cells[6].Value.ToString());
                frm.DateEnd = DateTime.Parse(row.Cells[7].Value.ToString());
                frm.edit = true;
                this.Hide();
                frm.ShowDialog();
                this.Dispose();
                                
            }
        }

        private void mtSearch_Click(object sender, EventArgs e)
        {
            Model1 db = new Model1();

            var result = from ce in db.CandidatesEducation
                         join c in db.Candidates on ce.CandidateID equals c.CandidateID
                         select new
                         {
                             ce.CandidateEducation,
                             ce.CandidateID,
                             Name = c.Name + " " + c.LastName,
                             ce.Institute,
                             ce.Level,
                             ce.DateBeg,
                             ce.DateEnd,
                             ce.Career,
                             c.Inactive
                         };

            if (mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(nw => nw.Inactive == false);
                dataGridView1.DataSource = result.ToList();
            }
            else if (mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(nw => nw.Inactive == true);
                dataGridView1.DataSource = result.ToList();
            }

            if (mtcanfilter.Text != "")
            {

                result = result.Where(c => c.CandidateID.ToString().Contains(mtcanfilter.Text) ||
                                  c.Name.Contains(mtcanfilter.Text));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtcanfilter.Text == "")
            {
                dataGridView1.DataSource = result.ToList();
            }
        }

        private void mtgoback_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuRecruiment frm = new MenuRecruiment();
            frm.ShowDialog();
            this.Dispose();
        }
    }
    
}
