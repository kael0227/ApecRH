﻿namespace ApecRH
{
    partial class CandidatesLanguaguesAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MtLanguage = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnsavequit = new MetroFramework.Controls.MetroButton();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.MtLevel = new MetroFramework.Controls.MetroComboBox();
            this.mtCandidate = new MetroFramework.Controls.MetroTextBox();
            this.mtcandwork = new MetroFramework.Controls.MetroTextBox();
            this.candidatesLanguagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesLanguagesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MtLanguage);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.btnsavequit);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.MtLevel);
            this.groupBox1.Controls.Add(this.mtCandidate);
            this.groupBox1.Controls.Add(this.mtcandwork);
            this.groupBox1.Location = new System.Drawing.Point(59, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 332);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "WorkExp";
            // 
            // MtLanguage
            // 
            this.MtLanguage.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesLanguagesBindingSource, "Languague", true));
            this.MtLanguage.FormattingEnabled = true;
            this.MtLanguage.ItemHeight = 23;
            this.MtLanguage.Items.AddRange(new object[] {
            "",
            "Ingles",
            "Frances",
            "Español"});
            this.MtLanguage.Location = new System.Drawing.Point(190, 140);
            this.MtLanguage.Name = "MtLanguage";
            this.MtLanguage.Size = new System.Drawing.Size(178, 29);
            this.MtLanguage.TabIndex = 30;
            this.MtLanguage.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(71, 192);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(38, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Level";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(71, 140);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(73, 19);
            this.metroLabel3.TabIndex = 27;
            this.metroLabel3.Text = "Languague";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(71, 95);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(81, 19);
            this.metroLabel2.TabIndex = 26;
            this.metroLabel2.Text = "CandidateID";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(71, 45);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(47, 19);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "CandE";
            this.metroLabel1.Visible = false;
            // 
            // btnsavequit
            // 
            this.btnsavequit.Location = new System.Drawing.Point(322, 253);
            this.btnsavequit.Name = "btnsavequit";
            this.btnsavequit.Size = new System.Drawing.Size(100, 23);
            this.btnsavequit.TabIndex = 24;
            this.btnsavequit.Text = "Save And Quit";
            this.btnsavequit.UseSelectable = true;
            this.btnsavequit.Visible = false;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(56, 253);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(190, 253);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // MtLevel
            // 
            this.MtLevel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesLanguagesBindingSource, "Level", true));
            this.MtLevel.FormattingEnabled = true;
            this.MtLevel.ItemHeight = 23;
            this.MtLevel.Items.AddRange(new object[] {
            "",
            "Begginer",
            "Intermediate",
            "Advanced"});
            this.MtLevel.Location = new System.Drawing.Point(192, 192);
            this.MtLevel.Name = "MtLevel";
            this.MtLevel.Size = new System.Drawing.Size(177, 29);
            this.MtLevel.TabIndex = 21;
            this.MtLevel.UseSelectable = true;
            // 
            // mtCandidate
            // 
            // 
            // 
            // 
            this.mtCandidate.CustomButton.Image = null;
            this.mtCandidate.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtCandidate.CustomButton.Name = "";
            this.mtCandidate.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtCandidate.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtCandidate.CustomButton.TabIndex = 1;
            this.mtCandidate.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtCandidate.CustomButton.UseSelectable = true;
            this.mtCandidate.CustomButton.Visible = false;
            this.mtCandidate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesLanguagesBindingSource, "CandidateID", true));
            this.mtCandidate.Lines = new string[0];
            this.mtCandidate.Location = new System.Drawing.Point(191, 95);
            this.mtCandidate.MaxLength = 32767;
            this.mtCandidate.Name = "mtCandidate";
            this.mtCandidate.PasswordChar = '\0';
            this.mtCandidate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtCandidate.SelectedText = "";
            this.mtCandidate.SelectionLength = 0;
            this.mtCandidate.SelectionStart = 0;
            this.mtCandidate.ShortcutsEnabled = true;
            this.mtCandidate.Size = new System.Drawing.Size(177, 23);
            this.mtCandidate.TabIndex = 19;
            this.mtCandidate.UseSelectable = true;
            this.mtCandidate.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtCandidate.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtCandidate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtcandwork
            // 
            // 
            // 
            // 
            this.mtcandwork.CustomButton.Image = null;
            this.mtcandwork.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtcandwork.CustomButton.Name = "";
            this.mtcandwork.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtcandwork.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtcandwork.CustomButton.TabIndex = 1;
            this.mtcandwork.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtcandwork.CustomButton.UseSelectable = true;
            this.mtcandwork.CustomButton.Visible = false;
            this.mtcandwork.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.candidatesLanguagesBindingSource, "CandidateEducation", true));
            this.mtcandwork.Lines = new string[0];
            this.mtcandwork.Location = new System.Drawing.Point(191, 45);
            this.mtcandwork.MaxLength = 32767;
            this.mtcandwork.Name = "mtcandwork";
            this.mtcandwork.PasswordChar = '\0';
            this.mtcandwork.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtcandwork.SelectedText = "";
            this.mtcandwork.SelectionLength = 0;
            this.mtcandwork.SelectionStart = 0;
            this.mtcandwork.ShortcutsEnabled = true;
            this.mtcandwork.Size = new System.Drawing.Size(177, 23);
            this.mtcandwork.TabIndex = 17;
            this.mtcandwork.UseSelectable = true;
            this.mtcandwork.Visible = false;
            this.mtcandwork.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtcandwork.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // candidatesLanguagesBindingSource
            // 
            this.candidatesLanguagesBindingSource.DataSource = typeof(ApecRH.Models.CandidatesLanguages);
            // 
            // CandidatesLanguaguesAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 459);
            this.Controls.Add(this.groupBox1);
            this.Name = "CandidatesLanguaguesAddAndEdit";
            this.Text = "CandidatesLanguaguesAddAndEdit";
            this.Load += new System.EventHandler(this.CandidatesLanguaguesAddAndEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.candidatesLanguagesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        public MetroFramework.Controls.MetroButton btnsavequit;
        private MetroFramework.Controls.MetroButton BtnCancel;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroComboBox MtLevel;
        private MetroFramework.Controls.MetroTextBox mtCandidate;
        public MetroFramework.Controls.MetroTextBox mtcandwork;
        private System.Windows.Forms.BindingSource candidatesLanguagesBindingSource;
        private MetroFramework.Controls.MetroComboBox MtLanguage;
    }
}