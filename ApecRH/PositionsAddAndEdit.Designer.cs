﻿namespace ApecRH
{
    partial class PositionsAddAndEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnsavequit = new MetroFramework.Controls.MetroButton();
            this.BtnCancel = new MetroFramework.Controls.MetroButton();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.McInactive = new MetroFramework.Controls.MetroCheckBox();
            this.McRisk = new MetroFramework.Controls.MetroComboBox();
            this.MtPosSalMax = new MetroFramework.Controls.MetroTextBox();
            this.mtPosName = new MetroFramework.Controls.MetroTextBox();
            this.mtPosSalMin = new MetroFramework.Controls.MetroTextBox();
            this.mtPosID = new MetroFramework.Controls.MetroTextBox();
            this.positionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.risksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.positionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.risksBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.BtnCancel);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.McInactive);
            this.groupBox1.Controls.Add(this.McRisk);
            this.groupBox1.Controls.Add(this.MtPosSalMax);
            this.groupBox1.Controls.Add(this.mtPosName);
            this.groupBox1.Controls.Add(this.mtPosSalMin);
            this.groupBox1.Controls.Add(this.mtPosID);
            this.groupBox1.Location = new System.Drawing.Point(33, 63);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 396);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Position";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(80, 301);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(52, 19);
            this.metroLabel6.TabIndex = 30;
            this.metroLabel6.Text = "Inactive";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(80, 256);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(31, 19);
            this.metroLabel5.TabIndex = 29;
            this.metroLabel5.Text = "Risk";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(80, 205);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(58, 19);
            this.metroLabel4.TabIndex = 28;
            this.metroLabel4.Text = "Sal Max.";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(80, 151);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(55, 19);
            this.metroLabel3.TabIndex = 27;
            this.metroLabel3.Text = "Sal Min.";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(80, 106);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(54, 19);
            this.metroLabel2.TabIndex = 26;
            this.metroLabel2.Text = "Position";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(80, 56);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(66, 19);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "PositionID";
            // 
            // btnsavequit
            // 
            this.btnsavequit.Location = new System.Drawing.Point(327, 34);
            this.btnsavequit.Name = "btnsavequit";
            this.btnsavequit.Size = new System.Drawing.Size(100, 23);
            this.btnsavequit.TabIndex = 24;
            this.btnsavequit.Text = "Save And Quit";
            this.btnsavequit.UseSelectable = true;
            this.btnsavequit.Visible = false;
            this.btnsavequit.Click += new System.EventHandler(this.btnsavequit_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(155, 341);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(96, 23);
            this.BtnCancel.TabIndex = 9;
            this.BtnCancel.Text = "Cancel";
            this.BtnCancel.UseSelectable = true;
            this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(289, 341);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(105, 23);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // McInactive
            // 
            this.McInactive.AutoSize = true;
            this.McInactive.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.positionsBindingSource, "Inactive", true));
            this.McInactive.Location = new System.Drawing.Point(200, 305);
            this.McInactive.Name = "McInactive";
            this.McInactive.Size = new System.Drawing.Size(64, 15);
            this.McInactive.TabIndex = 22;
            this.McInactive.Text = "Inactive";
            this.McInactive.UseSelectable = true;
            // 
            // McRisk
            // 
            this.McRisk.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.positionsBindingSource, "RiskID", true));
            this.McRisk.FormattingEnabled = true;
            this.McRisk.ItemHeight = 23;
            this.McRisk.Location = new System.Drawing.Point(200, 246);
            this.McRisk.Name = "McRisk";
            this.McRisk.Size = new System.Drawing.Size(177, 29);
            this.McRisk.TabIndex = 21;
            this.McRisk.UseSelectable = true;
            // 
            // MtPosSalMax
            // 
            // 
            // 
            // 
            this.MtPosSalMax.CustomButton.Image = null;
            this.MtPosSalMax.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.MtPosSalMax.CustomButton.Name = "";
            this.MtPosSalMax.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.MtPosSalMax.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.MtPosSalMax.CustomButton.TabIndex = 1;
            this.MtPosSalMax.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MtPosSalMax.CustomButton.UseSelectable = true;
            this.MtPosSalMax.CustomButton.Visible = false;
            this.MtPosSalMax.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.positionsBindingSource, "SalMax", true));
            this.MtPosSalMax.Lines = new string[0];
            this.MtPosSalMax.Location = new System.Drawing.Point(200, 201);
            this.MtPosSalMax.MaxLength = 32767;
            this.MtPosSalMax.Name = "MtPosSalMax";
            this.MtPosSalMax.PasswordChar = '\0';
            this.MtPosSalMax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.MtPosSalMax.SelectedText = "";
            this.MtPosSalMax.SelectionLength = 0;
            this.MtPosSalMax.SelectionStart = 0;
            this.MtPosSalMax.ShortcutsEnabled = true;
            this.MtPosSalMax.Size = new System.Drawing.Size(177, 23);
            this.MtPosSalMax.TabIndex = 20;
            this.MtPosSalMax.UseSelectable = true;
            this.MtPosSalMax.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.MtPosSalMax.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.MtPosSalMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtPosName
            // 
            // 
            // 
            // 
            this.mtPosName.CustomButton.Image = null;
            this.mtPosName.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtPosName.CustomButton.Name = "";
            this.mtPosName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtPosName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtPosName.CustomButton.TabIndex = 1;
            this.mtPosName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtPosName.CustomButton.UseSelectable = true;
            this.mtPosName.CustomButton.Visible = false;
            this.mtPosName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.positionsBindingSource, "Position", true));
            this.mtPosName.Lines = new string[0];
            this.mtPosName.Location = new System.Drawing.Point(200, 106);
            this.mtPosName.MaxLength = 32767;
            this.mtPosName.Name = "mtPosName";
            this.mtPosName.PasswordChar = '\0';
            this.mtPosName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtPosName.SelectedText = "";
            this.mtPosName.SelectionLength = 0;
            this.mtPosName.SelectionStart = 0;
            this.mtPosName.ShortcutsEnabled = true;
            this.mtPosName.Size = new System.Drawing.Size(177, 23);
            this.mtPosName.TabIndex = 19;
            this.mtPosName.UseSelectable = true;
            this.mtPosName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtPosName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtPosName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtName_KeyPress);
            // 
            // mtPosSalMin
            // 
            // 
            // 
            // 
            this.mtPosSalMin.CustomButton.Image = null;
            this.mtPosSalMin.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtPosSalMin.CustomButton.Name = "";
            this.mtPosSalMin.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtPosSalMin.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtPosSalMin.CustomButton.TabIndex = 1;
            this.mtPosSalMin.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtPosSalMin.CustomButton.UseSelectable = true;
            this.mtPosSalMin.CustomButton.Visible = false;
            this.mtPosSalMin.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.positionsBindingSource, "SalMin", true));
            this.mtPosSalMin.Lines = new string[0];
            this.mtPosSalMin.Location = new System.Drawing.Point(200, 151);
            this.mtPosSalMin.MaxLength = 32767;
            this.mtPosSalMin.Name = "mtPosSalMin";
            this.mtPosSalMin.PasswordChar = '\0';
            this.mtPosSalMin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtPosSalMin.SelectedText = "";
            this.mtPosSalMin.SelectionLength = 0;
            this.mtPosSalMin.SelectionStart = 0;
            this.mtPosSalMin.ShortcutsEnabled = true;
            this.mtPosSalMin.Size = new System.Drawing.Size(177, 23);
            this.mtPosSalMin.TabIndex = 18;
            this.mtPosSalMin.UseSelectable = true;
            this.mtPosSalMin.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtPosSalMin.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtPosSalMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // mtPosID
            // 
            // 
            // 
            // 
            this.mtPosID.CustomButton.Image = null;
            this.mtPosID.CustomButton.Location = new System.Drawing.Point(155, 1);
            this.mtPosID.CustomButton.Name = "";
            this.mtPosID.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.mtPosID.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.mtPosID.CustomButton.TabIndex = 1;
            this.mtPosID.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.mtPosID.CustomButton.UseSelectable = true;
            this.mtPosID.CustomButton.Visible = false;
            this.mtPosID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.positionsBindingSource, "PositionID", true));
            this.mtPosID.Lines = new string[0];
            this.mtPosID.Location = new System.Drawing.Point(200, 56);
            this.mtPosID.MaxLength = 32767;
            this.mtPosID.Name = "mtPosID";
            this.mtPosID.PasswordChar = '\0';
            this.mtPosID.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.mtPosID.SelectedText = "";
            this.mtPosID.SelectionLength = 0;
            this.mtPosID.SelectionStart = 0;
            this.mtPosID.ShortcutsEnabled = true;
            this.mtPosID.Size = new System.Drawing.Size(177, 23);
            this.mtPosID.TabIndex = 17;
            this.mtPosID.UseSelectable = true;
            this.mtPosID.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.mtPosID.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.mtPosID.Click += new System.EventHandler(this.mtPosID_Click);
            this.mtPosID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtEmployee_KeyPress);
            // 
            // positionsBindingSource
            // 
            this.positionsBindingSource.DataSource = typeof(ApecRH.Models.Positions);
            // 
            // risksBindingSource
            // 
            this.risksBindingSource.DataSource = typeof(ApecRH.Models.Risks);
            // 
            // PositionsAddAndEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 494);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnsavequit);
            this.Name = "PositionsAddAndEdit";
            this.Text = "PositionsAddAndEdit";
            this.Load += new System.EventHandler(this.PositionsAddAndEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.positionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.risksBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroCheckBox McInactive;
        private MetroFramework.Controls.MetroComboBox McRisk;
        private MetroFramework.Controls.MetroTextBox MtPosSalMax;
        private MetroFramework.Controls.MetroTextBox mtPosName;
        private MetroFramework.Controls.MetroTextBox mtPosSalMin;
        public MetroFramework.Controls.MetroTextBox mtPosID;
        private System.Windows.Forms.BindingSource risksBindingSource;
        private System.Windows.Forms.BindingSource positionsBindingSource;
        private MetroFramework.Controls.MetroButton BtnCancel;
        public MetroFramework.Controls.MetroButton btnsavequit;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}