﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class FormCandidates : MetroFramework.Forms.MetroForm
    {

        public bool addEmployee = false;
        public FormCandidates()
        {
            InitializeComponent();
        }


        private void mtAddP_Click(object sender, EventArgs e)
        {
            this.Hide();
            CandidatesAddAndEdit frm = new CandidatesAddAndEdit();
            frm.ShowDialog();
            this.Dispose();
        }

        private void FormCandidates_Load(object sender, EventArgs e)
        {
            mcInactivefilter.SelectedItem = "Active";
            using (Models.Model1 db = new Models.Model1())
            {
                var result = from cand in db.Candidates
                             join dep in db.Departments on cand.DepartmentID equals dep.DepartmentID
                             join pos in db.Positions on cand.PositionID equals pos.PositionID
                             join nat in db.Nationalities on cand.NationalityID equals nat.NationalitiesID
                             where cand.Inactive == false
                             select new
                             {
                                 cand.CandidateID,
                                 cand.Name,
                                 cand.LastName,
                                 cand.NationalID,
                                 cand.HireDate,
                                 cand.DepartmentID,
                                 dep.Department,
                                 cand.PositionID,
                                 pos.Position,
                                 cand.NationalityID,
                                 nat.NationName,
                                 cand.Salary,
                                 cand.Inactive,
                                 cand.Approved
                             };
                dataGridView1.DataSource = result.ToList();
            }
        }

       

        private void mtEdit_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            CandidatesAddAndEdit frm = new CandidatesAddAndEdit();
            frm.empID = int.Parse(row.Cells[0].Value.ToString());
            frm.empName = row.Cells[1].Value.ToString();
            frm.empLastName = row.Cells[2].Value.ToString();
            frm.empNationalID = row.Cells[3].Value.ToString();
            frm.empHireDate = DateTime.Parse(row.Cells[4].Value.ToString());
            frm.empDep = int.Parse(row.Cells[5].Value.ToString());
            frm.empPos = int.Parse(row.Cells[7].Value.ToString());
            frm.empNation = row.Cells[9].Value.ToString();
            frm.empSalary = double.Parse(row.Cells[11].Value.ToString());
            frm.inactive = bool.Parse(row.Cells[12].Value.ToString());
            frm.edit = true;
            frm.mtEmployee.Enabled = false;
            frm.btnsavequit.Hide();
            this.Hide();
            frm.ShowDialog();
            this.Dispose();
        }

        private void mtApprove_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DialogResult dr = MessageBox.Show("Are you sure do you want to approve this candidate?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                    ApecRH.Models.Employees emp = new Models.Employees();
                    emp.EmployeeID = int.Parse(row.Cells[0].Value.ToString());
                    emp.Name = row.Cells[1].Value.ToString();
                    emp.LastName = row.Cells[2].Value.ToString();
                    emp.NationalID = row.Cells[3].Value.ToString();
                    emp.HireDate = DateTime.Now;
                    emp.DepartmentRefID = int.Parse(row.Cells[5].Value.ToString());
                    emp.PositionID = int.Parse(row.Cells[7].Value.ToString());
                    emp.NationalitiesID = row.Cells[9].Value.ToString();
                    emp.Salary = double.Parse(row.Cells[11].Value.ToString());
                    emp.Inactive = bool.Parse(row.Cells[12].Value.ToString());

                    if (emp.Inactive == true)
                    {
                        MessageBox.Show("Sorry. You can't Approve an inactive candidate.");
                    }

                    else
                    {

                        using (Models.Model1 db = new Models.Model1())
                        {
                            db.Employees.Add(emp);



                            var candU = db.Candidates.Where(x => x.CandidateID == emp.EmployeeID).Select(x => x).FirstOrDefault();
                            candU.Inactive = true;
                            candU.Approved = true;
                            MessageBox.Show("Approved!");
                            db.SaveChanges();
                            db.Dispose();
                            this.Hide();
                            FormEmployee frm = new FormEmployee();
                            frm.ShowDialog();
                            this.Dispose();
                        }
                    }
                }




                catch (Exception ex)
                {

                    string errorMessage = ex.Message;
                }
            }
     }

        

        private void mtDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }

            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to delete?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {
                       
                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        var candD = db.Candidates.First(c => c.CandidateID == toBeDeleted);
                        //WorkExperience dependency validation
                        if (db.CandidatesWorkExperience.Any(cdwe => cdwe.CandidateID == toBeDeleted) || db.CandidatesEducation.Any(ce => ce.CandidateID == toBeDeleted) || db.CandidatesSkills.Any(cs => cs.CandidateID == toBeDeleted) || db.CandidatesLanguages.Any(cl => cl.CandidateID == toBeDeleted))
                        {
                            MessageBox.Show("Can't erase record. It has dependency");
                            return;
                        }
                        

                        
                        //Inactive validation
                        bool inactiveD = (from d in db.Candidates
                                          where d.CandidateID == toBeDeleted
                                          select d.Inactive).Single();
                        if(inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar Candidatos Inactivos. Contacte al supervisor");
                            return;
                        }
                        db.Candidates.Remove(candD);
                        db.SaveChanges();
                        var result = from cand in db.Candidates
                                     join dep in db.Departments on cand.DepartmentID equals dep.DepartmentID
                                     join pos in db.Positions on cand.PositionID equals pos.PositionID
                                     join nat in db.Nationalities on cand.NationalityID equals nat.NationalitiesID
                                     where cand.Inactive == false
                                     select new
                                     {
                                         cand.CandidateID,
                                         cand.Name,
                                         cand.LastName,
                                         cand.NationalID,
                                         cand.HireDate,
                                         cand.DepartmentID,
                                         dep.Department,
                                         cand.PositionID,
                                         pos.Position,
                                         cand.NationalityID,
                                         nat.NationName,
                                         cand.Salary,
                                         cand.Inactive,
                                         cand.Approved
                                     };
                        dataGridView1.DataSource = result.ToList();
                        

                    }
                }
            }

            catch (Exception ex)
            {

                string errorMessage = ex.Message;
                MessageBox.Show(ex.Message);
            }
        }

        private void mtSearch_Click(object sender, EventArgs e)
        {
            Models.Model1 db = new Models.Model1();
            //query to modify for filters

            var result = from cand in db.Candidates
                         join dep in db.Departments on cand.DepartmentID equals dep.DepartmentID
                         join pos in db.Positions on cand.PositionID equals pos.PositionID
                         join nat in db.Nationalities on cand.NationalityID equals nat.NationalitiesID
                         select new
                         {
                             cand.CandidateID,
                             cand.Name,
                             cand.LastName,
                             cand.NationalID,
                             cand.HireDate,
                             cand.DepartmentID,
                             dep.Department,
                             cand.PositionID,
                             pos.Position,
                             cand.NationalityID,
                             nat.NationName,
                             cand.Salary,
                             cand.Inactive,
                             cand.Approved
                         };

            if(mcInactivefilter.SelectedItem == "")
            {
                dataGridView1.DataSource = result.ToList();

            }
            else if(mcInactivefilter.SelectedItem.ToString() == "Active")
            {
                result = result.Where(c => c.Inactive == false);
                dataGridView1.DataSource = result.ToList();

            }
            else if(mcInactivefilter.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(c => c.Inactive == true);
                dataGridView1.DataSource = result.ToList();
            }
            if(mtcanfilter.Text != "")
            {

                result = result.Where(c => c.CandidateID.ToString().Contains(mtcanfilter.Text) ||
                                  c.Name.Contains(mtcanfilter.Text) ||
                                  c.LastName.Contains(mtcanfilter.Text));
                dataGridView1.DataSource = result.ToList();
            }
            else if (mtcanfilter.Text == "")
            {
                dataGridView1.DataSource = result.ToList();
            }


        }

        private void mtGoBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuRecruiment frm = new MenuRecruiment();
            frm.ShowDialog();
            this.Dispose();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DialogResult dr = MessageBox.Show("Are you sure do you want to disapprove this candidate?", "Confirmation", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    DataGridViewRow row = this.dataGridView1.SelectedRows[0];
                    ApecRH.Models.Employees emp = new Models.Employees();
                    emp.EmployeeID = int.Parse(row.Cells[0].Value.ToString());
                    

                        using (Models.Model1 db = new Models.Model1())
                        {
                         



                            var candU = db.Candidates.Where(x => x.CandidateID == emp.EmployeeID).Select(x => x).FirstOrDefault();
                            candU.Inactive = true;
                            candU.Approved = false;
                            MessageBox.Show("Disapproved!");
                            db.SaveChanges();
                            db.Dispose();
                        }
                    
                }




                catch (Exception ex)
                {

                    string errorMessage = ex.Message;
                }
            }
        }
    }
    
}
    


