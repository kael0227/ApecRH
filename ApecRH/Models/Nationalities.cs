﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
    public class Nationalities
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string NationalitiesID { get; set; }
        [StringLength(255)]
        public string NationName { get; set; }
  
        public bool Inactive { get; set; }
        public virtual ICollection<Employees> Employee { get; set; }
    }
}
