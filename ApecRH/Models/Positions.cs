﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
    public class Positions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PositionID { get; set; }
        [StringLength(255)]
        public string Position { get; set; }
        [ForeignKey("Risk")]
        public int RiskID { get; set; }
        public virtual Risks Risk { get; set; }
  
        public double SalMin { get; set;}
        public double SalMax { get; set; }
        public bool Inactive { get; set; }
        public virtual ICollection<Employees> Employee { get; set; }
        public virtual ICollection<CandidatesWorkExperience> CandidateWorkExperience { get; set; }
    }
}
