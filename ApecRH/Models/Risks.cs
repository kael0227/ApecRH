﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
    public class Risks
    {
      

        public int ID{ get; set; }

        public string RiskLevel { get; set; }

        public virtual ICollection<Positions> Position {get; set;}

    }
}

