﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
  public class Candidates
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CandidateID { get; set; }
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string LastName { get; set; }
  
        public string NationalID { get; set; }

        public DateTime HireDate { get; set; }
        //Department one to many relationship------------------------------------------
        [ForeignKey("Department")]
        public int DepartmentID { get; set; }
        public virtual Departments Department { get; set; }
        //Positions One to many---------------------------------------------------------
        [ForeignKey("Position")]
        public int PositionID { get; set; }
        public virtual Positions Position { get; set; }
        //Nationalities One to many---------------------------------------------------------
        [ForeignKey("Nationality")]
        public string NationalityID { get; set; }
        public virtual Nationalities Nationality { get; set; }

        public double Salary { get; set; }
        public bool Inactive { get; set; }
        public bool Approved { get; set; }
    }
}
