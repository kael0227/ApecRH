﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
    public class CandidatesEducation
    {
        [Key]

        public int CandidateEducation { get; set; }
        [ForeignKey("Candidate")]
        public int CandidateID { get; set; }
        public virtual Candidates Candidate { get; set; }
        [StringLength(255)]
        public string Institute { get; set; }
        [StringLength(255)]
        public string Career { get; set; }
        public string Level { get; set; }
        public DateTime DateBeg { get; set; }
        public DateTime DateEnd { get; set; }
    }
}
