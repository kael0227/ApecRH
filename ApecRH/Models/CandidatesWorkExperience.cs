﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApecRH.Models
{
    public class CandidatesWorkExperience
    {
        [Key]
        
        public int CandidateWorkExperience { get; set; }
        [ForeignKey("Candidate")]
        public int CandidateID { get; set; }
        public virtual Candidates Candidate { get; set; }
        [StringLength(255)]
        public string Organization { get; set; }

        [ForeignKey("Position")]
        public int PositionID { get; set; }
        public virtual Positions Position { get; set; }
        public DateTime DateBeg { get; set; }
        public DateTime DateEnd { get; set; }
        public double Salary { get; set; }


    }
}
