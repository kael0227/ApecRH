﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApecRH
{
    public partial class FormDepartments : MetroFramework.Forms.MetroForm
    {

        public FormDepartments()
        {
            InitializeComponent();
        }



        private void mtAdd_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (DepartmentsAddAndEdit frm = new DepartmentsAddAndEdit())
            {

                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void Departments_Load(object sender, EventArgs e)
        {
            mcincativefiler.SelectedItem = "Active";
            using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
            {

                var result = from Departments in db.Departments
                             where Departments.Inactive == false
                             select Departments;


                departmentsBindingSource.DataSource = result.ToList();



            }
        }



        private void btnSearch_Click(object sender, EventArgs e)
        {


            ApecRH.Models.Model1 db = new ApecRH.Models.Model1();

            var result = from Departments in db.Departments
                         select Departments;
            //Filtro de estado.
            if (mcincativefiler.SelectedItem == " ")
            {
                dataGridView1.DataSource = db.Departments.ToList();
            }




            if (mtfilterdp.Text == "")
            {

                departmentsBindingSource.DataSource = result.ToList();

            }
            else if (mtfilterdp.Text != "")
            {
                string dpwhere = mtfilterdp.Text;

                result = result.Where(Departments => Departments.DepartmentID.ToString().Contains(dpwhere) ||
                                                     Departments.Department.Contains(dpwhere));

                dataGridView1.DataSource = result.ToList();
            }


            if (mcincativefiler.SelectedItem.ToString() == "Inactive")
            {
                result = result.Where(Departments => (Departments.Inactive == true));

                dataGridView1.DataSource = result.ToList();
            }

            else if (mcincativefiler.SelectedItem.ToString() == "Active")
            {
                result = result.Where(Departments => (Departments.Inactive == false));
                dataGridView1.DataSource = result.ToList();
            }






        }

        private void metroTile4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            try
            {
                DialogResult dr = MessageBox.Show("Are you sure do you want to delete this record?", "Confirmation", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    using (ApecRH.Models.Model1 db = new ApecRH.Models.Model1())
                    {

                        var toBeDeleted = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                        if (db.Candidates.Any(p => p.DepartmentID == toBeDeleted))
                        {
                            MessageBox.Show("Can't erased record. It has dependency");
                            return;
                        }
                        var DepD = db.Departments.First(c => c.DepartmentID == toBeDeleted);
                        bool inactiveD = (from d in db.Departments
                                          where d.DepartmentID == toBeDeleted
                                          select d.Inactive).Single();
                        if (inactiveD == true)
                        {
                            MessageBox.Show("No se pueden borrar datos Inactivos. Contacte al supervisor");
                            return;
                        }
                        db.Departments.Remove(DepD);
                        db.SaveChanges();
                        var result = from Departments in db.Departments
                                     where Departments.Inactive == false
                                     select Departments;

                        dataGridView1.DataSource = result.ToList();


                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void MtEdit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please Select a row");
                return;
            }
            DataGridViewRow row = this.dataGridView1.SelectedRows[0];
            DepartmentsAddAndEdit frm = new DepartmentsAddAndEdit();
            frm.DepID = int.Parse(row.Cells[0].Value.ToString());
            frm.DepName = row.Cells[1].Value.ToString();
            frm.Inactive = bool.Parse(row.Cells[2].Value.ToString());
            frm.edit = true;
            frm.btnSaveAndQuit.Hide();
            frm.mtxtDepID.Enabled = false;
            this.Hide();
            frm.ShowDialog();
            this.Dispose();


        }

        private void mtGoback_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuAdministration frm = new MenuAdministration();
            frm.ShowDialog();
            this.Dispose();
        }
    }
}

