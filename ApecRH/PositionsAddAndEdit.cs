﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class PositionsAddAndEdit : MetroFramework.Forms.MetroForm
    {
        Positions Posn = new Positions();
        public int posID;
        public string posName;
        public double posSalMin;
        public double posSalMax;
        public int posRisk;
        public bool inactive;
        public bool editp = false;

        public PositionsAddAndEdit()
        {
            InitializeComponent();

        }


        public void clear()
        {
            mtPosID.Text = "";
            mtPosName.Text = "";
            MtPosSalMax.Text = "";
            mtPosSalMin.Text = "";
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            if (!ValidParameters())
                return;
            Model1 db = new Model1();
            if (editp == false)
            {



                posID = int.Parse(mtPosID.Text);
                posName = mtPosName.Text;
                posSalMin = double.Parse(mtPosSalMin.Text);
                posSalMax = double.Parse(MtPosSalMax.Text);
                posRisk = int.Parse(McRisk.SelectedValue.ToString());
                if (McInactive.Checked)
                {
                    inactive = true;

                }
                else
                {

                    inactive = false;
                }
                if (mtPosID.Text == "" || mtPosName.Text == "" || MtPosSalMax.Text == "" || mtPosSalMin.Text == "")
                {
                    MessageBox.Show("Please Fill all the text boxes");
                    return;
                }
                Posn.PositionID = posID;
                if (db.Positions.Any(p => p.PositionID == posID))
                {
                    MessageBox.Show("Code exists. Please enter a different code.");
                    mtPosID.Focus();
                    return;
                }
                Posn.Position = posName;
                if (db.Positions.Any(p => p.Position == posName))
                {
                    MessageBox.Show("Position exists. Please enter a different code.");
                    mtPosName.Focus();
                    return;
                }
                Posn.SalMin = posSalMin;
                Posn.SalMax = posSalMax;
                Posn.RiskID = posRisk;
                Posn.Inactive = inactive;


                try
                {
                    db.Positions.Add(Posn);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    clear();


                }
                catch (Exception)
                {

                    MessageBox.Show("Registro o Clave Duplicada. Por favor instertar diferente ID.");

                }


            }
            else
            {
                try
                {

                    var PosU = db.Positions.Where(x => x.PositionID == posID).Select(x => x).FirstOrDefault();
                    posName = mtPosName.Text;
                    posSalMin = double.Parse(mtPosSalMin.Text);
                    posSalMax = double.Parse(MtPosSalMax.Text);
                    posRisk = int.Parse(McRisk.SelectedValue.ToString());
                    inactive = McInactive.Checked;
                    posRisk = int.Parse(McRisk.SelectedValue.ToString());


                    PosU.PositionID = posID;
                    PosU.Position = posName;
                    PosU.SalMin = posSalMin;
                    PosU.SalMax = posSalMax;
                    PosU.Inactive = inactive;
                    PosU.RiskID = posRisk;

                    if (db.Candidates.Any(p => p.PositionID == posID) && inactive == true)
                    {
                        MessageBox.Show("Can't Inactive record. It has dependency");
                        return;
                    }
                    if (mtPosID.Text == "" || mtPosName.Text == "" || MtPosSalMax.Text == "" || mtPosSalMin.Text == "")
                    {
                        MessageBox.Show("Please Fill all the text boxes");
                        return;
                    }
                   
                    
                    db.SaveChanges();
                    db.Dispose();
                    this.Hide();
                    FormPositions frm2 = new FormPositions();
                    frm2.ShowDialog();
                    this.Dispose();


                }
                catch (Exception ex)
                {

                    string errormsg = ex.Message;
                }

            }
        }





        public bool ValidParameters()
        {
            try
            {
                posSalMin = double.Parse(mtPosSalMin.Text);
                posSalMax = double.Parse(MtPosSalMax.Text);
                if (posSalMin < 0 || posSalMax < 0)
                    throw new Exception("Valor no puede ser menor que cero");

                if (posSalMax < posSalMin)
                    throw new Exception("Salario Maximo no puede ser menor que Salario Minimo.");
            }
            catch (Exception ex)
            {

                MessageBox.Show("Salario Maximo no puede ser menor que Salario Minimo.");
                return false;
            }
            return true;
        }

        private void mtPosID_Click(object sender, EventArgs e)
        {

        }

        private void PositionsAddAndEdit_Load(object sender, EventArgs e)
        {
            

            using (Model1 db = new Model1())
            {
                McRisk.DataSource = db.Risks.ToList();
                McRisk.ValueMember = "ID";
                McRisk.DisplayMember = "RiskLevel";
            }
            if (editp == true)
            {
                mtPosID.Text = posID.ToString();
                mtPosName.Text = posName;
                mtPosSalMin.Text = posSalMin.ToString();
                MtPosSalMax.Text = posSalMax.ToString();
                McInactive.Checked = inactive;
                McRisk.SelectedValue= posRisk;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            using (FormPositions frm = new FormPositions())
            {
                frm.ShowDialog();
            }
            this.Dispose();
        }

        private void btnsavequit_Click(object sender, EventArgs e)
        {
            if (!ValidParameters())
                return;





            posID = int.Parse(mtPosID.Text);
            posName = mtPosName.Text;
            posSalMin = double.Parse(mtPosSalMin.Text);
            posSalMax = double.Parse(MtPosSalMax.Text);
            posRisk = int.Parse(McRisk.SelectedValue.ToString());
            if (McInactive.Checked)
            {
                inactive = true;

            }
            else
            {

                inactive = false;
            }

            Posn.PositionID = posID;
            Posn.Position = posName;
            Posn.SalMin = posSalMin;
            Posn.SalMax = posSalMax;
            Posn.RiskID = posRisk;
            Posn.Inactive = inactive;


            using (Model1 db = new Model1())
            {
                try
                {
                    db.Positions.Add(Posn);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Se ha guardado correctamente");
                    this.Hide();
                    using (FormPositions frm = new FormPositions())
                    {
                        frm.ShowDialog();
                    }
                    this.Dispose();


                }
                catch (Exception)
                {

                    MessageBox.Show("Registro o Clave Duplicada. Por favor instertar diferente ID.");

                }

            }
        }
        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));
        }

    }
}
