﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApecRH.Models;

namespace ApecRH
{
    public partial class CandidatesWorkExperienceAddAndEdit : MetroFramework.Forms.MetroForm
    {
        public int CandweID;
        public int CandidateID;
        public string Org;
        public int PosID;
        public DateTime DateBeg;
        public DateTime DateEnd;
        public double Salary;
        public bool edit = false;

        public CandidatesWorkExperienceAddAndEdit()
        {
            InitializeComponent();
        }
        public void clear()
        {
            mtCandidate.Text = "";
            mtOrganization.Text = "";
            MtSalary.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            if (edit == false)
            {
                try
                {
                    Model1 db = new Model1();
                    CandidateID = int.Parse(mtCandidate.Text);
                    Org = mtOrganization.Text;
                    PosID = int.Parse(MtPosition.SelectedValue.ToString());
                    DateBeg = DateTime.Parse(MtDateBeg.Text);
                    DateEnd = DateTime.Parse(MtDateEnd.Text);
                    if (DateBeg > DateEnd)
                    {

                        MessageBox.Show("DateBeg cannot be greater than DateEnd.");
                        return;
                    }
                    Salary = double.Parse(MtSalary.Text);

                    CandidatesWorkExperience cwe = new CandidatesWorkExperience();

                    if (mtCandidate.Text == "" || mtOrganization.Text == "")
                    {

                        MessageBox.Show("Please fill all the textbox");
                        return;

                    }
                    cwe.CandidateID = CandidateID;
                    cwe.Organization = Org;
                    cwe.PositionID = PosID;
                    cwe.DateBeg = DateBeg;
                    cwe.DateEnd = DateEnd;
                    cwe.Salary = Salary;


                    db.CandidatesWorkExperience.Add(cwe);
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                    clear();



                }
                catch (Exception ex)
                {

                    string errormessage = ex.Message;
                }



            }
            else
            {


                using (Model1 db = new Model1())
                {
                    var CanweU = db.CandidatesWorkExperience.Where(x => x.CandidateWorkExperience == CandweID).Select(x => x).FirstOrDefault();
                    CandidateID = int.Parse(mtCandidate.Text);
                    Org = mtOrganization.Text;
                    PosID = int.Parse(MtPosition.SelectedValue.ToString());
                    DateBeg = DateTime.Parse(MtDateBeg.Text);
                    DateEnd = DateTime.Parse(MtDateEnd.Text);
                    Salary = double.Parse(MtSalary.Text);



                    if (mtCandidate.Text == "" || mtOrganization.Text == "")
                    {

                        MessageBox.Show("Please fill all the textbox");
                        return;

                    }
                    CanweU.CandidateID = CandidateID;
                    CanweU.Organization = Org;
                    CanweU.PositionID = PosID;
                    CanweU.DateBeg = DateBeg;
                    CanweU.DateEnd = DateEnd;
                    CanweU.Salary = Salary;
                    db.SaveChanges();
                    db.Dispose();
                    MessageBox.Show("Saved!");
                }


            }
        }

        private void CandidatesWorkExperienceAddAndEdit_Load(object sender, EventArgs e)
        {
           
            using (Model1 db = new Model1())
            {
                if (!db.Candidates.Any(c => c.Inactive == false))
                {
                    MessageBox.Show("There's no any Candidate please insert one.");
                    FormCandidates frm = new FormCandidates();
                    this.Hide();
                    frm.ShowDialog();
                    this.Dispose();
                }
                var query = from c in db.Candidates
                            where c.Inactive == false
                            select c.CandidateID.ToString();



                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                source.AddRange(query.ToArray());
                this.mtCandidate.AutoCompleteMode = AutoCompleteMode.Suggest;
                this.mtCandidate.AutoCompleteSource = AutoCompleteSource.CustomSource;
                this.mtCandidate.AutoCompleteCustomSource = source;





                var result = from p in db.Positions
                             where p.Inactive == false
                             select p;
                MtPosition.DataSource = result.ToList();
                MtPosition.ValueMember = "PositionID";
                MtPosition.DisplayMember = "Position";

                if (edit == true)
                {

                    mtcandwork.Text = CandweID.ToString();
                    mtCandidate.Text = CandidateID.ToString();
                    mtOrganization.Text = Org;
                    MtPosition.Text = PosID.ToString();
                    MtDateBeg.Text = DateBeg.ToString();
                    MtDateEnd.Text = DateEnd.ToString();
                    MtSalary.Text = Salary.ToString();
                }
            }

        }
        private void mtEmployee_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void mtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back || char.IsWhiteSpace(e.KeyChar));
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormCandidatesWorkExperience frm = new FormCandidatesWorkExperience();
            frm.ShowDialog();
            this.Dispose();
        }
    }
}
